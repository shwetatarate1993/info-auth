export default class UserProfile {
    userId: string;
    password: string;
    name: string;
    givenName: string;
    familyName: string;
    userStatus: string;
    middleName: string;
    email: string;
    phoneNumber: number;
    picture: string;
    birthDate: Date;
    gender: string;
    emailVerified: boolean;
    phoneNumberVerified: boolean;
    address: string;
    nickName: string;
    preferredUserName: string;
    profile: string;
    website: string;
    zoneInfo: string;
    locale: string;
    createdAt: Date;
    updatedAt: Date;
    code: string;
    loginAttempt: number;
    refreshToken: string;
    constructor(userId: string, password: string, name: string, givenName: string, familyName: string, userStatus: string, middleName: string, email: string, phoneNumber: number, picture: string, birthDate: Date, gender: string, emailVerified: boolean, phoneNumberVerified: boolean, address: string, nickName: string, preferredUserName: string, profile: string, website: string, zoneInfo: string, locale: string, createdAt: Date, updatedAt: Date, code: string, loginAttempt: number, refreshToken: string);
}
//# sourceMappingURL=user.profile.model.d.ts.map