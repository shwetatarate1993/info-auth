"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const knex_config_1 = require("knex-config");
const constants_1 = require("../constants");
const knex = knex_config_1.knexClient(info_commons_1.config.get(constants_1.DB), constants_1.METADB, info_commons_1.DIALECT);
const fields = ['USERID as USER_ID', 'USERNAME as USER_NAME', 'PASSWORD', 'FIRSTNAME as FIRST_NAME',
    'LASTNAME as LAST_NAME', 'USERSTATUS as USER_STATUS', 'EMAILADDRESS as EMAIL_ADDRESS',
    'VERIFICATIONKEY as VERIFICATION_KEY', 'USER_LOGIN_ID', 'USER_KEY', 'CONTACTNUMBER as CONTACT_NUMBER',
    'PROFILESTATUS as PROFILE_STATUS', 'PHOTO', 'MOBILE_NUMBER', 'DATE_OF_BIRTH', 'GENDER', 'CASTE',
    'OFFICE_PHONE_NUMBER', 'QUALIFICATION', 'MIDDLE_NAME', 'POSITION_TITLE', 'COMPANY_EMAIL_ADDREES',
    'COMPANY_LINKEDIN_ID', 'PASSWORD_CHANGED', 'PASSWORD_STATUS'];
class User {
    static find(id) {
        return knex('USERDETAIL')
            .where('USERID', id)
            .select(...fields)
            .then((rows) => rows.map((row) => {
            // adding a key as EXPIRES_AT by adding 30 days to PASSWORD_CHANGED value to show password expiry
            const date = new Date(Object(row).PASSWORD_CHANGED);
            date.setDate(date.getDate() + 30);
            Object(row).EXPIRES_AT = date;
            return new User(info_commons_1.parse(row));
        }))
            .catch((err) => { throw err; });
    }
    static async fetchUser(userName, password) {
        const rows = await knex('USERDETAIL')
            .where({ USERNAME: userName, PASSWORD: password })
            .select(...fields);
        return rows.map((row) => {
            // adding a key as EXPIRES_AT by adding 30 days to PASSWORD_CHANGED value to show password expiry
            const date = new Date(Object(row).PASSWORD_CHANGED);
            date.setDate(date.getDate() + 30);
            Object(row).EXPIRES_AT = date;
            return new User(info_commons_1.parse(row));
        });
        return null;
    }
    static fetchUserByUserName(userName) {
        return knex('USERDETAIL')
            .where('USERNAME', userName)
            .select(...fields)
            .then((rows) => rows.map((row) => {
            // adding a key as EXPIRES_AT by adding 30 days to PASSWORD_CHANGED value to show password expiry
            const date = new Date(Object(row).PASSWORD_CHANGED);
            date.setDate(date.getDate() + 30);
            Object(row).EXPIRES_AT = date;
            return new User(info_commons_1.parse(row));
        }))
            .catch((err) => { throw err; });
    }
    static findAll() {
        return knex('USERDETAIL')
            .select(...fields)
            .then((rows) => rows.map((row) => new User(info_commons_1.parse(row))))
            .catch((err) => {
            throw err;
        });
    }
    constructor(data) {
        Object.assign(this, data);
    }
}
exports.default = User;
//# sourceMappingURL=user.model.js.map