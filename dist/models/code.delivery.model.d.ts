export default class CodeDelivery {
    attributeName: string;
    deliveryMedium: string;
    destination: string;
    constructor(attributeName: string, deliveryMedium: string, destination: string);
}
//# sourceMappingURL=code.delivery.model.d.ts.map