"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class UserDetail {
    constructor(userAuthId, password, userStatus, email, code, loginAttempt, emailVerified, refreshToken, createdAt, updatedAt) {
        this.userAuthId = userAuthId;
        this.password = password;
        this.userStatus = userStatus;
        this.email = email;
        this.emailVerified = emailVerified;
        this.code = code;
        this.loginAttempt = loginAttempt;
        this.refreshToken = refreshToken;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        Object.freeze(this);
    }
}
exports.default = UserDetail;
//# sourceMappingURL=userdetail.model.js.map