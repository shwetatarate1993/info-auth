export default class UserDetail {
    userAuthId: string;
    password: string;
    userStatus: string;
    email: string;
    code: string;
    emailVerified: boolean;
    loginAttempt: number;
    refreshToken: string;
    createdAt: Date;
    updatedAt: Date;
    constructor(userAuthId: string, password: string, userStatus: string, email: string, code: string, loginAttempt: number, emailVerified: boolean, refreshToken: string, createdAt: Date, updatedAt: Date);
}
//# sourceMappingURL=userdetail.model.d.ts.map