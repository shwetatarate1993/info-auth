"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class TokenResponse {
    constructor(idToken, accessToken, refreshToken, expiresIn, tokenType, userStatus) {
        this.idToken = idToken;
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.expiresIn = expiresIn;
        this.tokenType = tokenType;
        this.userStatus = userStatus;
        Object.freeze(this);
    }
}
exports.default = TokenResponse;
//# sourceMappingURL=token.response.model.js.map