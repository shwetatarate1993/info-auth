declare class User {
    static find(id: number): Promise<User[] | Error>;
    static fetchUser(userName: string, password: string): Promise<User[]>;
    static fetchUserByUserName(userName: string): Promise<User[] | Error>;
    static findAll(): Promise<User[] | Error>;
    constructor(data: User);
}
export default User;
//# sourceMappingURL=user.model.d.ts.map