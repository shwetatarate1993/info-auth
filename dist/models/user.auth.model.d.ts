import Knex from 'knex';
declare class UserAuth {
    static knex: Knex;
    static find(id: string): Promise<UserAuth[]>;
    static fetchUser(emailID: string, password: string): Promise<UserAuth>;
    static fetchUserByEmailAddress(emailId: string): Promise<UserAuth[]>;
    static findAll(): Promise<UserAuth[]>;
    constructor(data: UserAuth);
}
export default UserAuth;
//# sourceMappingURL=user.auth.model.d.ts.map