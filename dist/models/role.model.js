"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const knex_config_1 = require("knex-config");
const constants_1 = require("../constants");
const knex = knex_config_1.knexClient(info_commons_1.config.get(constants_1.DB), constants_1.METADB, info_commons_1.DIALECT);
const fields = ['ROLE.ROLEID as ROLE_ID', 'ROLENAME as ROLE_NAME', 'CREATEDBY as CREATED_BY',
    'UPDATEDBY as UPDATED_BY', 'CREATIONDATE as CREATION_DATE', 'UPDATIONDATE as UPDATION_DATE',
    'ISDELETED as IS_DELETED', 'DELETIONDATE as DELETION_DATE', 'ISPROJECTLEVEL as IS_PROJECT_LEVEL',
    'ROLEDESC as ROLE_DESC', 'ROLE.PROJECTID as PROJECT_ID'];
class Role {
    static find(id) {
        return knex('ROLE')
            .select(...fields)
            .leftJoin('USERMAPPING', 'USERMAPPING.ROLEID', 'ROLE.ROLEID')
            .where('USERID', id)
            .then((rows) => rows.map((row) => new Role(info_commons_1.parse(row))))
            .catch((err) => {
            throw err;
        });
    }
    static findAll() {
        return knex('ROLE')
            .select(...fields)
            .then((rows) => rows.map((row) => new Role(info_commons_1.parse(row))))
            .catch((err) => {
            throw err;
        });
    }
    constructor(data) {
        Object.assign(this, data);
    }
}
exports.default = Role;
//# sourceMappingURL=role.model.js.map