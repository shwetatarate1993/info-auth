export default class TokenResponse {
    idToken: string;
    accessToken: string;
    refreshToken: string;
    expiresIn: number;
    tokenType: string;
    userStatus: string;
    constructor(idToken: string, accessToken: string, refreshToken: string, expiresIn: number, tokenType: string, userStatus: string);
}
//# sourceMappingURL=token.response.model.d.ts.map