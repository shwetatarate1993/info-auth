"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const knex_config_1 = require("knex-config");
const constants_1 = require("../constants");
const knex = knex_config_1.knexClient(info_commons_1.config.get(constants_1.DB), constants_1.METADB, info_commons_1.DIALECT);
const fields = ['USERMAPPINGID as USERMAPPING_ID', 'USERID as USER_ID', 'PROJECTID as PROJECT_ID',
    'ROLEID as ROLE_ID', 'ITEMID as ITEM_ID', 'MAPPINGTYPE as MAPPING_TYPE', 'STATUS',
    'DEFAULTFLAG as DEFAULT_FLAG'];
class UserMapping {
    static find(id) {
        return knex('USERMAPPING')
            .where('ROLEID', id)
            .select(...fields)
            .then((rows) => rows.map((row) => {
            return new UserMapping(info_commons_1.parse(row));
        }))
            .catch((err) => { throw err; });
    }
    static findAll() {
        return knex('USERMAPPING')
            .select(...fields)
            .then((rows) => rows.map((row) => {
            return new UserMapping(info_commons_1.parse(row));
        }))
            .catch((err) => { throw err; });
    }
    constructor(data) {
        Object.assign(this, data);
    }
}
exports.default = UserMapping;
//# sourceMappingURL=usermapping.model.js.map