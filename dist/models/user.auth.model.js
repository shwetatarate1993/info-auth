"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const fields = ['PASSWORD', 'EMAIL_ADDRESS',
    'VERIFICATION_KEY', 'USER_STATUS', 'LOGIN_ATTEMPT'];
class UserAuth {
    constructor(data) {
        Object.assign(this, data);
    }
    static find(id) {
        return UserAuth.knex('USER_AUTH')
            .where('USER_AUTH_ID', id)
            .select(...fields)
            .then((rows) => rows.map((row) => {
            // adding a key as EXPIRES_AT by adding 30 days to PASSWORD_CHANGED value to show password expiry
            const date = new Date(Object(row).PASSWORD_CHANGED);
            date.setDate(date.getDate() + 30);
            Object(row).EXPIRES_AT = date;
            return new UserAuth(info_commons_1.parse(row));
        }))
            .catch((err) => { throw err; });
    }
    static async fetchUser(emailID, password) {
        const rows = await UserAuth.knex('USER_AUTH')
            .where({ EMAIL_ADDRESS: emailID, PASSWORD: password })
            .select(...fields);
        return rows.map((row) => {
            // adding a key as EXPIRES_AT by adding 30 days to PASSWORD_CHANGED value to show password expiry
            const date = new Date(Object(row).PASSWORD_CHANGED);
            date.setDate(date.getDate() + 30);
            Object(row).EXPIRES_AT = date;
            return new UserAuth(info_commons_1.parse(row));
        });
        return null;
    }
    static fetchUserByEmailAddress(emailId) {
        return UserAuth.knex('USER_AUTH')
            .where('EMAIL_ADDRESS', emailId)
            .select(...fields)
            .then((rows) => rows.map((row) => {
            // adding a key as EXPIRES_AT by adding 30 days to PASSWORD_CHANGED value to show password expiry
            const date = new Date(Object(row).PASSWORD_CHANGED);
            date.setDate(date.getDate() + 30);
            Object(row).EXPIRES_AT = date;
            return new UserAuth(info_commons_1.parse(row));
        }))
            .catch((err) => { throw err; });
    }
    static findAll() {
        return UserAuth.knex('USER_AUTH')
            .select(...fields)
            .then((rows) => rows.map((row) => new UserAuth(info_commons_1.parse(row))))
            .catch((err) => {
            throw err;
        });
    }
}
exports.default = UserAuth;
//# sourceMappingURL=user.auth.model.js.map