declare class UserMapping {
    static find(id: string): Promise<UserMapping[] | Error>;
    static findAll(): Promise<UserMapping[] | Error>;
    constructor(data: UserMapping);
}
export default UserMapping;
//# sourceMappingURL=usermapping.model.d.ts.map