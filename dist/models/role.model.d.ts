declare class Role {
    static find(id: string): Promise<Role[]>;
    static findAll(): Promise<Role[]>;
    constructor(data: Role);
}
export default Role;
//# sourceMappingURL=role.model.d.ts.map