declare class SubProject {
    static find(id: string): Promise<SubProject[]>;
    constructor(data: SubProject);
}
export default SubProject;
//# sourceMappingURL=subproject.model.d.ts.map