"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// import {knex, parse} from '../config/knex.configg';
const info_commons_1 = require("info-commons");
const knex_config_1 = require("knex-config");
const constants_1 = require("../constants");
const knex = knex_config_1.knexClient(info_commons_1.config.get(constants_1.DB), constants_1.METADB, info_commons_1.DIALECT);
const fields = ['SUB_PROJECT_UUID', 'SUB_PROJECT_ID', 'PROJECT_ID', 'EFFECTIVE_DATE', 'CANCEL_DATE',
    'SUB_PROJECT_NAME', 'IS_DEFAULT', 'SUB_PROJECT_ORDER'];
class SubProject {
    static find(id) {
        return knex('SUB_PROJECT')
            .where('PROJECT_ID', id)
            .select(...fields)
            .then((rows) => rows.map((row) => {
            return new SubProject(info_commons_1.parse(row));
        }))
            .catch((err) => { throw err; });
    }
    constructor(data) {
        Object.assign(this, data);
    }
}
exports.default = SubProject;
//# sourceMappingURL=subproject.model.js.map