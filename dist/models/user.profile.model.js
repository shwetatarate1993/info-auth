"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class UserProfile {
    constructor(userId, password, name, givenName, familyName, userStatus, middleName, email, phoneNumber, picture, birthDate, gender, emailVerified, phoneNumberVerified, address, nickName, preferredUserName, profile, website, zoneInfo, locale, createdAt, updatedAt, code, loginAttempt, refreshToken) {
        this.userId = userId;
        this.password = password;
        this.name = name;
        this.givenName = givenName;
        this.familyName = familyName;
        this.userStatus = userStatus;
        this.middleName = middleName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.picture = picture;
        this.birthDate = birthDate;
        this.gender = gender;
        this.emailVerified = emailVerified;
        this.phoneNumberVerified = phoneNumberVerified;
        this.address = address;
        this.nickName = nickName;
        this.preferredUserName = preferredUserName;
        this.profile = profile;
        this.website = website;
        this.zoneInfo = zoneInfo;
        this.locale = locale;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.code = code;
        this.loginAttempt = loginAttempt;
        this.refreshToken = refreshToken;
        Object.freeze(this);
    }
}
exports.default = UserProfile;
//# sourceMappingURL=user.profile.model.js.map