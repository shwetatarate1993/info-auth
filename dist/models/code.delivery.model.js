"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class CodeDelivery {
    constructor(attributeName, deliveryMedium, destination) {
        this.attributeName = attributeName;
        this.deliveryMedium = deliveryMedium;
        this.destination = destination;
        Object.freeze(this);
    }
}
exports.default = CodeDelivery;
//# sourceMappingURL=code.delivery.model.js.map