export interface User {
    email: string;
    email_verified: boolean;
    phone_number: number;
    phone_number_verified: boolean;
    address: string;
    name: string;
    family_name: string;
    given_name: string;
    middle_name: string;
    nickname: string;
    preferred_username: string;
    profile: string;
    picture: string;
    website: string;
    gender: string;
    birthdate: Date;
    zoneinfo: string;
    locale: string;
    updated_at: Date;
    created_at: Date;
    iss: string;
    sub: string;
    aud: string;
}
//# sourceMappingURL=user.type.d.ts.map