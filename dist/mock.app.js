"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mock_express_1 = __importDefault(require("mock-express"));
const controllers_1 = require("./controllers");
const mockApp = mock_express_1.default();
mockApp.get('/users', (req, res, next) => {
    controllers_1.userController.fetchAllUsers(req, res, next);
});
mockApp.get('/users/:id', (req, res, next) => {
    controllers_1.userController.fetchUserByID(req, res, next);
});
exports.default = mockApp;
//# sourceMappingURL=mock.app.js.map