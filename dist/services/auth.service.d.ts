import { ErrorResponse } from 'info-commons';
import { CodeDeliveries, TokenResponse, UserDetail } from '../models';
import AuthProvider from '../providers/auth/auth.provider.interface';
import { TokenResponseType } from '../types/token.response.type';
export declare class AuthService {
    authProvider: AuthProvider;
    constructor(configDB: any, databaseId: string, dialect: string);
    authentication(userName: string, password: string, relayState: string): Promise<string>;
    fetchUserByEmailID(emailID: string): Promise<UserDetail>;
    desktopAuthorization(scope: string, clientId: string, usrName: string, password: string): Promise<TokenResponseType | ErrorResponse>;
    authorization(scope: string, clientId: string, usrName: string, password: string): Promise<TokenResponse>;
    refreshToken(scope: string, clientId: string, refreshToken: string): Promise<TokenResponse>;
    createUser(reqBody: any, action: string): Promise<number>;
    updateUserStatus(userName: string, code: string): Promise<ErrorResponse>;
    forgotPassword(userName: string): Promise<CodeDeliveries>;
    resetPassword(userName: string, newPassword: string, code: string): Promise<ErrorResponse>;
    updatePassword(userName: string, oldPassword: string, newPassword: string): Promise<ErrorResponse>;
    resendCode(userName: string): Promise<CodeDeliveries>;
}
declare const getAuthService: (configDB: any, databaseId: string, dialect: string) => AuthService;
export default getAuthService;
//# sourceMappingURL=auth.service.d.ts.map