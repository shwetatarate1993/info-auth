"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const jwt = __importStar(require("jsonwebtoken"));
const v4_1 = __importDefault(require("uuid/v4"));
const config_1 = __importDefault(require("../config"));
const constants_1 = require("../constants");
const models_1 = require("../models");
const providers_1 = require("../providers");
const encryption_1 = require("../utilities/encryption");
const mailing_1 = require("../utilities/mailing");
class AuthService {
    constructor(configDB, databaseId, dialect) {
        this.authProvider = providers_1.getAuthProvider(configDB, databaseId, dialect);
    }
    async authentication(userName, password, relayState) {
        return await this.authProvider.authentication(userName, password, relayState);
    }
    async fetchUserByEmailID(emailID) {
        return this.authProvider.fetchUserByEmailID(emailID);
    }
    async desktopAuthorization(scope, clientId, usrName, password) {
        const userDetail = await this.authProvider.fetchUserByEmailID(usrName);
        if (userDetail === null) {
            return null;
        }
        const encrypPassword = encryption_1.verifyingKey(userDetail.password, password);
        let tokenResponse;
        if (userDetail.password !== encrypPassword) {
            tokenResponse = null;
            if (userDetail.loginAttempt === 3) {
                // Update User Status to LOCKED
                await this.authProvider.updateUserStatus(usrName, constants_1.ErrorReasonEnum.LOCKED);
                tokenResponse = new models_1.TokenResponse(null, null, null, 3600, 'Bearer', constants_1.ErrorReasonEnum.LOCKED);
                const verificationErrorResponse = await verifyUserStatus(tokenResponse);
                if (verificationErrorResponse !== null) {
                    return verificationErrorResponse;
                }
            }
            // Increment Login Attempt
            if (tokenResponse === null) {
                await this.authProvider.incrementLoginAttempt(usrName);
                const invalidCredentialResponse = await invalidCredential();
                return invalidCredentialResponse;
            }
        }
        else {
            // Successfull Login, So Reset Login Attempt to 0, if required
            if (userDetail.loginAttempt !== 0) {
                await this.authProvider.resetLoginAttempt(usrName);
            }
            const userPayload = await processScope(userDetail, scope, clientId);
            const token = jwt.sign(userPayload, info_commons_1.config.get(info_commons_1.SECRET), { expiresIn: 14400 });
            tokenResponse = new models_1.TokenResponse(token, token, userDetail.refreshToken, 14400, 'Bearer', userDetail.userStatus);
            const verificationErrorResponse = await verifyUserStatus(tokenResponse);
            if (verificationErrorResponse !== null) {
                return verificationErrorResponse;
            }
            const responseJson = {};
            responseJson.id_token = tokenResponse.idToken;
            responseJson.access_token = tokenResponse.accessToken;
            responseJson.refresh_token = tokenResponse.refreshToken;
            responseJson.expires_in = tokenResponse.expiresIn;
            responseJson.token_type = tokenResponse.tokenType;
            return responseJson;
        }
    }
    async authorization(scope, clientId, usrName, password) {
        const userDetail = await this.authProvider.fetchUserByEmailID(usrName);
        if (userDetail === null) {
            return null;
        }
        const encrypPassword = encryption_1.verifyingKey(userDetail.password, password);
        if (userDetail.password !== encrypPassword) {
            if (userDetail.loginAttempt === 3) {
                // Update User Status to LOCKED
                await this.authProvider.updateUserStatus(usrName, constants_1.ErrorReasonEnum.LOCKED);
                return new models_1.TokenResponse(null, null, null, 3600, 'Bearer', constants_1.ErrorReasonEnum.LOCKED);
            }
            // Increment Login Attempt
            await this.authProvider.incrementLoginAttempt(usrName);
            return null;
        }
        else {
            // Successfull Login, So Reset Login Attempt to 0, if required
            if (userDetail.loginAttempt !== 0) {
                await this.authProvider.resetLoginAttempt(usrName);
            }
            const userPayload = await processScope(userDetail, scope, clientId);
            const token = jwt.sign(userPayload, info_commons_1.config.get(info_commons_1.SECRET), { expiresIn: 3600 });
            return new models_1.TokenResponse(token, token, userDetail.refreshToken, 3600, 'Bearer', userDetail.userStatus);
        }
    }
    async refreshToken(scope, clientId, refreshToken) {
        const jwtPayload = JSON.parse(JSON.stringify(jwt.verify(refreshToken, info_commons_1.config.get(info_commons_1.SECRET))));
        const user = await this.authProvider.fetchUserByEmailID(jwtPayload.userName);
        if (user === null) {
            return null;
        }
        let status = 'Valid Token';
        if (refreshToken !== user.refreshToken) {
            status = 'Token Mismatch';
        }
        const userPayload = await processScope(user, scope, clientId);
        const token = jwt.sign(userPayload, info_commons_1.config.get(info_commons_1.SECRET), { expiresIn: 3600 });
        return new models_1.TokenResponse(token, token, refreshToken, 3600, 'Bearer', status);
    }
    async createUser(reqBody, action) {
        reqBody.CREATED_AT = new Date();
        reqBody.USER_STATUS = action === 'signup' ? 'PENDING_VERIFICATION' : 'PENDING_CHANGE_PASSWORD';
        reqBody.LOGIN_ATTEMPT = 0;
        const refreshToken = jwt.sign({ userName: reqBody.emailAddress }, info_commons_1.config.get(info_commons_1.SECRET), { expiresIn: '1y' });
        reqBody.REFRESH_TOKEN = refreshToken;
        delete reqBody.action;
        delete reqBody.firstName;
        const encrypPassword = encryption_1.keyEncryption(reqBody.password);
        reqBody.PASSWORD = encrypPassword;
        return await this.authProvider.createUser(reqBody);
    }
    async updateUserStatus(userName, code) {
        const user = await this.authProvider.fetchUserByEmailID(userName);
        if (user === null) {
            return null;
        }
        const verificationKey = encryption_1.verifyingKey(user.code, code);
        const error = new info_commons_1.ErrorResponse();
        error.errors = [];
        if (verificationKey !== user.code) {
            error.code = 400;
            error.message = 'Invalid Verification Code';
            error.errors.push({
                domain: 'global', reason: constants_1.ErrorReasonEnum.INVALID_CODE,
                message: 'Invalid Code', locationType: 'Request Body',
                location: 'code', extendedHelp: 'http://request/help',
            });
            return error;
        }
        await this.authProvider.updateUserStatus(userName, 'ACTIVE');
        return error;
    }
    async forgotPassword(userName) {
        const code = v4_1.default().substring(0, 8);
        const encrypVerificationKey = encryption_1.keyEncryption(code);
        const result = await this.authProvider.updateVerificationKey(userName, encrypVerificationKey);
        const user = await this.authProvider.fetchUserByEmailID(userName);
        if (user === null) {
            return null;
        }
        mailing_1.postMail(userName, 'Password Reset Code', 'Your Password Reset Code: ' + code);
        const codeDelivery = new models_1.CodeDelivery('email', 'EMAIL', userName);
        return new models_1.CodeDeliveries([codeDelivery]);
    }
    async resetPassword(userName, newPassword, code) {
        const user = await this.authProvider.fetchUserByEmailID(userName);
        if (user === null) {
            return null;
        }
        const encrypVerificationKey = encryption_1.verifyingKey(user.code, code);
        const error = new info_commons_1.ErrorResponse();
        error.errors = [];
        if (encrypVerificationKey !== user.code) {
            error.code = 400;
            error.message = 'Invalid Passcode';
            error.errors.push({
                domain: 'global', reason: constants_1.ErrorReasonEnum.INVALID_PWD_RESET_CODE,
                message: 'Invalid Code', locationType: 'Request Body',
                location: 'code', extendedHelp: 'http://request/help',
            });
            return error;
        }
        const encrypPassword = encryption_1.keyEncryption(newPassword);
        await this.authProvider.resetPassword(userName, encrypPassword);
        return error;
    }
    async updatePassword(userName, oldPassword, newPassword) {
        const user = await this.authProvider.fetchUserByEmailID(userName);
        if (user === null) {
            return null;
        }
        const encryptedPassword = encryption_1.verifyingKey(user.password, oldPassword);
        const error = new info_commons_1.ErrorResponse();
        error.errors = [];
        if (encryptedPassword !== user.password) {
            error.code = 400;
            error.message = 'Invalid Old Password';
            error.errors.push({
                domain: 'global', reason: constants_1.ErrorReasonEnum.INVALID_OLD_PWD,
                message: 'Invalid Old Password', locationType: 'Request Body',
                location: 'Old Password', extendedHelp: 'http://request/help',
            });
            return error;
        }
        const encrypPassword = encryption_1.keyEncryption(newPassword);
        await this.authProvider.resetPassword(userName, encrypPassword);
        return error;
    }
    async resendCode(userName) {
        const result = await this.authProvider.updateVerificationKey(userName, v4_1.default().substring(0, 8));
        const user = await this.authProvider.fetchUserByEmailID(userName);
        if (user === null) {
            return null;
        }
        mailing_1.postMail(user.email, 'User Verification Code', 'Verification Code: ' + user.code + '\n' +
            'Link: ' + config_1.default.get('links').usr_uri + user.userAuthId + '/code/' + user.code);
        const codeDelivery = new models_1.CodeDelivery('email', 'EMAIL', user.email);
        return new models_1.CodeDeliveries([codeDelivery]);
    }
}
exports.AuthService = AuthService;
const processScope = async (user, scope, clientId) => {
    const userPayload = {};
    const scopeArray = scope.replace(/\s\s+/g, constants_1.SPACE).split(constants_1.SPACE);
    if (scopeArray.includes(constants_1.EMAIL)) {
        userPayload.email = user.email;
        userPayload.email_verified = user.emailVerified ? true : false;
    }
    // if (scopeArray.includes(PHONE)) {
    //     userPayload.phone_number = user.phoneNumber;
    //     userPayload.phone_number_verified = user.phoneNumberVerified ? true : false;
    // }
    // if (scopeArray.includes(ADDRESS)) {
    //     userPayload.address = user.address;
    // }
    if (scopeArray.includes(constants_1.PROFILE)) {
        userPayload.name = user.email;
    }
    userPayload.created_at = user.createdAt;
    userPayload.iss = 'https://localhost:3306.com';
    userPayload.sub = user.userAuthId;
    userPayload.aud = clientId;
    return userPayload;
};
const invalidCredential = async () => {
    const error = new info_commons_1.ErrorResponse();
    error.code = 403;
    error.message = 'Invalid Credential';
    error.errors = [{
            domain: 'global', reason: constants_1.ErrorReasonEnum.INVALID_CREDENTIAL,
            message: 'Invalid userName or password', locationType: 'Request Parameter',
            location: 'userName or password', extendedHelp: 'http://request/help',
        }];
    return error;
};
const verifyUserStatus = async (tokenResponse) => {
    const errors = [];
    let hasError = false;
    if (tokenResponse.userStatus === 'PENDING_VERIFICATION') {
        // Type alias declaration
        const err = {};
        err.reason = constants_1.ErrorReasonEnum.PENDING_VERIFICATION;
        err.message = 'User verification is pending';
        errors.push(err);
        hasError = true;
    }
    if (tokenResponse.userStatus === 'DEACTIVATED') {
        // Type alias declaration
        const err = {};
        err.reason = constants_1.ErrorReasonEnum.DEACTIVATED;
        err.message = 'User has been deactivated';
        errors.push(err);
        hasError = true;
    }
    if (tokenResponse.userStatus === 'LOCKED') {
        // Type alias declaration
        const err = {};
        err.reason = constants_1.ErrorReasonEnum.LOCKED;
        err.message = 'Multiple Invalid Login Attempts';
        errors.push(err);
        hasError = true;
    }
    if (tokenResponse.userStatus === 'PENDING_CHANGE_PASSWORD') {
        // Type alias declaration
        const err = {};
        err.reason = constants_1.ErrorReasonEnum.PENDING_CHANGE_PASSWORD;
        err.message = 'User is not Active';
        errors.push(err);
        hasError = true;
    }
    if (hasError) {
        const error = new info_commons_1.ErrorResponse();
        error.code = 422;
        error.message = 'Incomplete User Profile';
        error.errors = errors;
        return error;
    }
    return null;
};
const getAuthService = (configDB, databaseId, dialect) => {
    return new AuthService(configDB, databaseId, dialect);
};
exports.default = getAuthService;
//# sourceMappingURL=auth.service.js.map