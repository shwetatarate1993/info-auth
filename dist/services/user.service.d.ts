import { Response } from 'express';
import { ErrorResponse } from 'info-commons';
import { Role, SubProject, User, UserMapping } from '../models';
import UserProvider from '../providers/user/user.provider.interface';
export declare class UserService {
    userProvider: UserProvider;
    constructor();
    createUser(reqBody: any, action: string): Promise<number>;
    updateUserStatus(userName: string, code: string): Promise<ErrorResponse>;
    updateUserByID(reqBody: any, id: string): Promise<number>;
    updateAllUsers(reqBody: any): Promise<number>;
    postMail(data: string, token: string, res: Response): Promise<any>;
    forgotPassword(id: string): Promise<User[]>;
    fetchUserByID(id: string | number): Promise<User[] | Error>;
    fetchUserByUserName(userName: string): Promise<User[] | Error>;
    fetchAllUsers(): Promise<User[] | Error>;
    fetchRolesByUser(id: string): Promise<Role[]>;
    fetchUsersByRoleID(id: string): Promise<UserMapping>;
    fetchSubProjectsByProjectID(projectId: string): Promise<SubProject>;
    fetchAllRoles(): Promise<Role[]>;
    fetchAllUserMappings(): Promise<UserMapping[] | Error>;
    createRole(reqBody: any): Promise<number>;
    updateRoleByID(reqBody: any, id: string): Promise<number>;
    createUserMapping(reqBody: any): Promise<number>;
    deleteUserByID(id: string): Promise<number>;
    deleteRoleById(id: string): Promise<number>;
    deleteUserMappingByUserId(id: string): Promise<number>;
    deleteUserMappingByRoleId(id: string): Promise<number>;
    deleteUserMapping(id: string): Promise<number>;
}
declare const _default: UserService;
export default _default;
//# sourceMappingURL=user.service.d.ts.map