"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const jwt = __importStar(require("jsonwebtoken"));
const constants_1 = require("../constants");
const providers_1 = require("../providers");
const encryption_1 = require("../utilities/encryption");
class UserService {
    constructor() {
        this.userProvider = providers_1.getUserProvider();
    }
    async createUser(reqBody, action) {
        reqBody.CREATED_AT = new Date();
        reqBody.USERSTATUS = action === 'signup' ? 'PENDING_VERIFICATION' : 'PENDING_CHANGE_PASSWORD';
        reqBody.LOGIN_ATTEMPT = 0;
        const refreshToken = jwt.sign({ userName: reqBody.userName }, info_commons_1.config.get(info_commons_1.SECRET), { expiresIn: '1y' });
        reqBody.REFRESH_TOKEN = refreshToken;
        delete reqBody.action;
        const encrypPassword = encryption_1.keyEncryption(reqBody.password);
        reqBody.PASSWORD = encrypPassword;
        return await this.userProvider.createUser(reqBody);
    }
    async updateUserStatus(userName, code) {
        const user = await providers_1.getAuthProvider(null, '', '').fetchUserByEmailID(userName);
        const verificationKey = encryption_1.verifyingKey(user.code, code);
        if (user === null) {
            return null;
        }
        const error = new info_commons_1.ErrorResponse();
        error.errors = [];
        if (verificationKey !== user.code) {
            error.code = 400;
            error.message = 'Invalid Verification Code';
            error.errors.push({
                domain: 'global', reason: constants_1.ErrorReasonEnum.INVALID_CODE,
                message: 'Invalid Code', locationType: 'Request Body',
                location: 'code', extendedHelp: 'http://request/help',
            });
            return error;
        }
        await this.userProvider.updateUserStatus(userName, 'ACTIVE');
        return error;
    }
    async updateUserByID(reqBody, id) {
        return this.userProvider.updateUserByID(reqBody, id);
    }
    async updateAllUsers(reqBody) {
        return this.userProvider.updateAllUsers(reqBody);
    }
    async postMail(data, token, res) {
        return this.userProvider.postMail(data, token, res);
    }
    async forgotPassword(id) {
        return this.userProvider.forgotPassword(id);
    }
    async fetchUserByID(id) {
        return this.userProvider.fetchUserByID(id);
    }
    async fetchUserByUserName(userName) {
        return this.userProvider.fetchUserByUserName(userName);
    }
    async fetchAllUsers() {
        return this.userProvider.fetchAllUsers();
    }
    async fetchRolesByUser(id) {
        return this.userProvider.fetchRolesByUser(id);
    }
    async fetchUsersByRoleID(id) {
        return this.userProvider.fetchUsersByRoleID(id);
    }
    async fetchSubProjectsByProjectID(projectId) {
        return this.userProvider.fetchSubProjectsByProjectID(projectId);
    }
    async fetchAllRoles() {
        return this.userProvider.fetchAllRoles();
    }
    async fetchAllUserMappings() {
        return this.userProvider.fetchAllUserMappings();
    }
    async createRole(reqBody) {
        return this.userProvider.createRole(reqBody);
    }
    async updateRoleByID(reqBody, id) {
        return this.userProvider.updateRoleByID(reqBody, id);
    }
    async createUserMapping(reqBody) {
        return this.userProvider.createUserMapping(reqBody);
    }
    async deleteUserByID(id) {
        return this.userProvider.deleteUserByID(id);
    }
    async deleteRoleById(id) {
        return this.userProvider.deleteRoleById(id);
    }
    async deleteUserMappingByUserId(id) {
        return this.userProvider.deleteUserMappingByUserId(id);
    }
    async deleteUserMappingByRoleId(id) {
        return this.userProvider.deleteUserMappingByRoleId(id);
    }
    async deleteUserMapping(id) {
        return this.userProvider.deleteUserMapping(id);
    }
}
exports.UserService = UserService;
exports.default = new UserService();
//# sourceMappingURL=user.service.js.map