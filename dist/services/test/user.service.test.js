"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const user_request_1 = __importDefault(require("../../mockdata/user.request"));
const user_sqlitedb_1 = require("../../mockdata/user.sqlitedb");
const services_1 = require("../../services");
test.skip('Create User with mandatory & valid inputs', async () => {
    await user_sqlitedb_1.tableSetup();
    const responseData = await services_1.userService.createUser(user_request_1.default.createUserRequest, 'signup');
    expect(responseData).toEqual(4);
    const user = await services_1.userService.fetchUserByID(1114);
    expect(Object(user[0]).userId).toEqual(1114);
});
test.skip('Update User with mandatory & valid inputs', async () => {
    let user = await services_1.userService.fetchUserByID(1114);
    expect(Object(user[0]).lastName).toEqual('D');
    await services_1.userService.updateUserByID(user_request_1.default.updateUserRequest, '1114');
    user = await services_1.userService.fetchUserByID(1114);
    expect(Object(user[0]).lastName).toEqual('Dhekwar');
});
test.skip('Update All Users with mandatory & valid inputs', async () => {
    let user = await services_1.userService.fetchUserByID(1114);
    expect(Object(user[0]).password).toEqual('abc');
    await services_1.userService.updateAllUsers(user_request_1.default.updateAllUsersRequest);
    user = await services_1.userService.fetchUserByID(1114);
    expect(Object(user[0]).password).toEqual('pwd');
});
test.skip('Fetch User By Id', async () => {
    const user = await services_1.userService.fetchUserByID(1111);
    expect(Object(user[0]).firstName).toEqual('Ankur');
});
test.skip('Fetch All Users', async () => {
    const user = await services_1.userService.fetchAllUsers();
    expect(user.length).toEqual(4);
});
test.skip('Fetch Roles By User', async () => {
    const roles = await services_1.userService.fetchRolesByUser('1111');
    expect(roles.length).toEqual(2);
});
test.skip('Fetch All Roles', async () => {
    const roles = await services_1.userService.fetchAllRoles();
    expect(roles.length).toEqual(3);
});
test.skip('Create Role', async () => {
    const responseData = await services_1.userService.createRole(user_request_1.default.createRoleRequest);
    expect(responseData).toEqual(4);
});
test.skip('Update Role', async () => {
    await services_1.userService.updateRoleByID(user_request_1.default.updateRoleRequest, '4');
    const roles = await services_1.userService.fetchAllRoles();
    expect(Object(roles[3]).roleName).toEqual('Database Administrator');
});
test.skip('Fetch All UserMappings', async () => {
    const userMappings = await services_1.userService.fetchAllUserMappings();
    expect(userMappings.length).toEqual(3);
});
test.skip('Delete User By Id', async () => {
    let user = await services_1.userService.fetchAllUsers();
    expect(user.length).toEqual(4);
    await services_1.userService.deleteUserByID('1114');
    user = await services_1.userService.fetchAllUsers();
    expect(user.length).toEqual(3);
});
test.skip('Delete Role By Id', async () => {
    let roles = await services_1.userService.fetchAllRoles();
    expect(roles.length).toEqual(4);
    await services_1.userService.deleteRoleById('4');
    roles = await services_1.userService.fetchAllRoles();
    expect(roles.length).toEqual(3);
});
test.skip('Delete UserMapping By Id', async () => {
    let userMappings = await services_1.userService.fetchAllUserMappings();
    expect(userMappings.length).toEqual(3);
    await services_1.userService.deleteUserMapping('3');
    userMappings = await services_1.userService.fetchAllUserMappings();
    expect(userMappings.length).toEqual(2);
});
test.skip('Delete UserMapping By User Id', async () => {
    let userMappings = await services_1.userService.fetchAllUserMappings();
    expect(userMappings.length).toEqual(2);
    await services_1.userService.deleteUserMappingByUserId('1111');
    userMappings = await services_1.userService.fetchAllUserMappings();
    expect(userMappings.length).toEqual(0);
});
test.skip('Delete UserMapping By Role Id', async () => {
    let userMappings = await services_1.userService.fetchAllUserMappings();
    expect(userMappings.length).toEqual(0);
    await services_1.userService.deleteUserMappingByRoleId('2');
    userMappings = await services_1.userService.fetchAllUserMappings();
    expect(userMappings.length).toEqual(0);
});
//# sourceMappingURL=user.service.test.js.map