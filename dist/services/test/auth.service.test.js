"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const sinon_1 = __importDefault(require("sinon"));
const constants_1 = require("../../constants");
const user_sqlitedb_1 = require("../../mockdata/user.sqlitedb");
const models_1 = require("../../models");
const services_1 = require("../../services");
const mailingService = __importStar(require("../../utilities/mailing"));
const pwdEncrypted = 'e534a11775e3e9416e0e45a2193960f4$e6db998ac053572a0' +
    'e0dca0e62d6af076513f717def07cdddf96ba3680a85df6';
const authService = services_1.getAuthService(info_commons_1.config.get(constants_1.DB), constants_1.MOCK, 'sqlite3');
describe('AUTHORIZATION', () => {
    test('Authorization with User Not Found', async () => {
        await user_sqlitedb_1.tableSetup();
        const fetchUserDetailMock = sinon_1.default.mock(authService.authProvider);
        fetchUserDetailMock.expects('fetchUserByEmailID').returns(null);
        fetchUserDetailMock.expects('incrementLoginAttempt').returns(null);
        const tokenResponse = await authService.authorization('openid', '1', 'guptaankur798@gmail.com', 'pass');
        sinon_1.default.assert.match(tokenResponse, null);
        fetchUserDetailMock.restore();
    });
    test('User send Invalid Email address with Valid Password', async () => {
        const tokenResponse = await authService.authorization('openid', '1', 'guptaankur79@gmail.com', 'pwd');
        sinon_1.default.assert.match(tokenResponse, null);
    });
    test('User send Valid Username with Invalid Password', async () => {
        const tokenResponse = await authService.authorization('openid', '1', 'guptaankur798@gmail.com', 'pass');
        sinon_1.default.assert.match(tokenResponse, null);
    });
    test('Multiple Attempts with Invalid Credential', async () => {
        const user = new models_1.UserDetail(null, pwdEncrypted, null, 'guptaankur798@gmail.com', null, 3, null, null, null, null);
        const fetchUserDetailMock = sinon_1.default.mock(authService.authProvider);
        fetchUserDetailMock.expects('fetchUserByEmailID').atLeast(1).returns(user);
        fetchUserDetailMock.expects('updateUserStatus').atLeast(1).returns(1);
        const tokenResponse = await authService.authorization('openid', '1', 'guptaankur798@gmail.com', 'pass');
        sinon_1.default.assert.match(tokenResponse.userStatus, constants_1.ErrorReasonEnum.LOCKED);
        fetchUserDetailMock.restore();
    });
    test('Authorization with success response', async () => {
        const user = new models_1.UserDetail(null, pwdEncrypted, 'ACTIVE', null, null, 0, null, null, null, null);
        const fetchUserDetailMock = sinon_1.default.mock(authService.authProvider);
        fetchUserDetailMock.expects('fetchUserByEmailID').returns(user);
        const tokenResponse = await authService.authorization('openid', '1', 'guptaankur798@gmail.com', 'pwd');
        sinon_1.default.assert.match(tokenResponse.userStatus, 'ACTIVE');
        fetchUserDetailMock.restore();
    });
});
describe('FORGOT PASSWORD', () => {
    test('Forgot Password with User Not Found', async () => {
        const updateVerificationKeyMock = sinon_1.default.mock(authService.authProvider);
        updateVerificationKeyMock.expects('updateVerificationKey').returns(0);
        const fetchUserByUserNameMock = sinon_1.default.mock(authService.authProvider);
        fetchUserByUserNameMock.expects('fetchUserByEmailID').returns(null);
        const result = await authService.forgotPassword('guptaankur798@gmail.com');
        sinon_1.default.assert.match(result, null);
        updateVerificationKeyMock.restore();
        fetchUserByUserNameMock.restore();
    });
    test('Forgot Password with User Found', async () => {
        const updateVerificationKeyMock = sinon_1.default.mock(authService.authProvider);
        updateVerificationKeyMock.expects('updateVerificationKey').returns(0);
        const user = new models_1.UserDetail(null, pwdEncrypted, null, 'guptaankur798@gmail.com', null, null, null, null, null, null);
        const fetchUserByUserNameMock = sinon_1.default.mock(authService.authProvider);
        fetchUserByUserNameMock.expects('fetchUserByEmailID').returns(user);
        const postMailMock = sinon_1.default.mock(mailingService);
        postMailMock.expects('postMail').returns(null);
        const codeDeliveries = await authService.forgotPassword('guptaankur798@gmail.com');
        sinon_1.default.assert.match(codeDeliveries.codeDeliveries[0].attributeName, 'email');
        sinon_1.default.assert.match(codeDeliveries.codeDeliveries[0].destination, 'guptaankur798@gmail.com');
        updateVerificationKeyMock.restore();
        fetchUserByUserNameMock.restore();
        postMailMock.restore();
    });
});
describe('RESET PASSWORD', () => {
    test('Reset Password with User Not Found', async () => {
        const fetchUserByUserNameMock = sinon_1.default.mock(authService.authProvider);
        fetchUserByUserNameMock.expects('fetchUserByEmailID').returns(null);
        const result = await authService.resetPassword('abc', 'newPass', 'code');
        sinon_1.default.assert.match(result, null);
        fetchUserByUserNameMock.restore();
    });
    test('Invalid Password Reset Code', async () => {
        const user = new models_1.UserDetail(null, null, null, null, 'abc', null, null, null, null, null);
        const fetchUserByUserNameMock = sinon_1.default.mock(authService.authProvider);
        fetchUserByUserNameMock.expects('fetchUserByEmailID').returns(user);
        const error = await authService.resetPassword('guptaankur798@gmail.com', 'newPass', 'code');
        sinon_1.default.assert.match(error.code, 400);
        sinon_1.default.assert.match(error.errors.length, 1);
        fetchUserByUserNameMock.restore();
    });
    test('Reset Password Successfully', async () => {
        const user = new models_1.UserDetail(null, null, null, null, pwdEncrypted, null, null, null, null, null);
        const fetchUserByUserNameMock = sinon_1.default.mock(authService.authProvider);
        fetchUserByUserNameMock.expects('fetchUserByEmailID').returns(user);
        const resetPasswordMock = sinon_1.default.mock(authService.authProvider);
        resetPasswordMock.expects('resetPassword').returns(1);
        const errorResponse = await authService.resetPassword('guptaankur798@gmail.com', 'newPass', 'pwd');
        sinon_1.default.assert.match(errorResponse.errors.length, 0);
        fetchUserByUserNameMock.restore();
        resetPasswordMock.restore();
    });
});
describe('UPDATE PASSWORD', () => {
    test('Update Password with User Not Found', async () => {
        const fetchUserByUserNameMock = sinon_1.default.mock(authService.authProvider);
        fetchUserByUserNameMock.expects('fetchUserByEmailID').returns(null);
        const errorResponse = await authService.updatePassword('guptaankur798@gmail.com', 'oldPass', 'newPass');
        sinon_1.default.assert.match(errorResponse, null);
        fetchUserByUserNameMock.restore();
    });
    test('Invalid Old Password', async () => {
        const user = new models_1.UserDetail(null, 'userPassword', null, null, null, null, null, null, null, null);
        const fetchUserByUserNameMock = sinon_1.default.mock(authService.authProvider);
        fetchUserByUserNameMock.expects('fetchUserByEmailID').returns(user);
        const error = await authService.updatePassword('guptaankur798@gmail.com', 'oldPass', 'newPass');
        sinon_1.default.assert.match(error.code, 400);
        sinon_1.default.assert.match(error.errors.length, 1);
        fetchUserByUserNameMock.restore();
    });
    test('Update Password Successfully', async () => {
        const user = new models_1.UserDetail(null, pwdEncrypted, null, null, null, null, null, null, null, null);
        const fetchUserByUserNameMock = sinon_1.default.mock(authService.authProvider);
        fetchUserByUserNameMock.expects('fetchUserByEmailID').returns(user);
        const resetPasswordMock = sinon_1.default.mock(authService.authProvider);
        resetPasswordMock.expects('resetPassword').returns(1);
        const error = await authService.updatePassword('guptaankur798@gmail.com', 'pwd', 'userPassword');
        sinon_1.default.assert.match(error.errors.length, 0);
        fetchUserByUserNameMock.restore();
        resetPasswordMock.restore();
    });
});
describe('RESEND CODE', () => {
    test('Resend Code with User Not Found', async () => {
        const fetchUserByUserNameMock = sinon_1.default.mock(authService.authProvider);
        fetchUserByUserNameMock.expects('fetchUserByEmailID').returns(null);
        const result = await authService.resendCode('guptaankur798@gmail.com');
        sinon_1.default.assert.match(result, null);
        fetchUserByUserNameMock.restore();
    });
    test('Resend Code Successfully', async () => {
        const user = new models_1.UserDetail(null, null, null, 'ag@gmail.com', null, null, null, null, null, null);
        const fetchUserByUserNameMock = sinon_1.default.mock(authService.authProvider);
        fetchUserByUserNameMock.expects('fetchUserByEmailID').returns(user);
        const postMailMock = sinon_1.default.mock(mailingService);
        postMailMock.expects('postMail').returns(null);
        const codeDeliveries = await authService.resendCode('guptaankur798@gmail.com');
        sinon_1.default.assert.match(codeDeliveries.codeDeliveries[0].destination, 'ag@gmail.com');
        fetchUserByUserNameMock.restore();
        postMailMock.restore();
    });
});
//# sourceMappingURL=auth.service.test.js.map