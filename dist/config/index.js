"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const convict_1 = __importDefault(require("convict"));
const development_1 = __importDefault(require("./env/development"));
const production_1 = __importDefault(require("./env/production"));
const serverurl = 'http://ec2-3-87-133-52.compute-1.amazonaws.com:8901';
// Define a schema
const config = convict_1.default({
    provider: {
        default: 'default',
    },
    emailHost: {
        default: 'ec2-34-201-146-30.compute-1.amazonaws.com',
    },
    emailPort: {
        default: 8899,
    },
    env: {
        doc: 'The application environment.',
        format: ['production', 'development', 'test'],
        default: 'development',
        env: 'NODE_ENV',
    },
    secret: {
        doc: 'Secret key',
        format: '*',
        default: 'defaultsecret',
        env: 'SECRET_KEY',
    },
    ip: {
        doc: 'The IP address to bind.',
        format: 'ipaddress',
        default: '127.0.0.1',
        env: 'IP_ADDRESS',
    },
    port: {
        doc: 'The port to bind.',
        format: 'port',
        default: 8901,
        env: 'PORT',
        arg: 'port',
    },
    authTable: 'USERDETAIL',
    links: {
        user_uri: serverurl + '/v1/users/',
        usermapping_uri: serverurl + '/v1/userMappings/',
        role_uri: serverurl + '/v1/roles/',
        unlock_password: serverurl + '/v1/authentication/recovery/unlock',
        change_password: serverurl + '/v1/authentication/credentials/change_password',
        cancel: serverurl + '/v1/authentication/cancel',
        extendedHelp: 'http://docs.domain.ext/solution',
        usr_uri: serverurl + '/v1/user/',
    },
});
// Load environment dependent configuration
const envName = config.get('env');
switch (envName) {
    case 'production':
        config.load(production_1.default);
        break;
    case 'development':
        config.load(development_1.default);
        break;
    default:
    // do nothing
}
// Perform validation
config.validate({ allowed: 'strict' });
exports.default = config;
//# sourceMappingURL=index.js.map