import AuthProvider from '../providers/auth/auth.provider.interface';
import UserProvider from '../providers/user/user.provider.interface';
export { default as getAuthDefaultProvider } from '../providers/auth/auth.default.provider';
export { default as userDefaultProvider } from '../providers/user/user.default.provider';
export declare const getAuthProvider: (configDB: any, databaseId: string, dialect: string) => AuthProvider;
export declare const getUserProvider: () => UserProvider;
//# sourceMappingURL=index.d.ts.map