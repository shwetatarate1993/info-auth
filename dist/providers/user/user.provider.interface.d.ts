import { Response } from 'express';
import { Role, SubProject, User, UserMapping } from '../../models';
export default interface UserProvider {
    createUser(reqBody: any): Promise<number>;
    updateUserStatus(userName: string, status: string): Promise<number>;
    updateUserByID(reqBody: string, id: string): Promise<number>;
    updateAllUsers(reqBody: string): Promise<number>;
    postMail(data: string, token: string, res: Response): Promise<Response>;
    forgotPassword(id: string): Promise<User[]>;
    fetchUserByID(id: string | number): Promise<User[] | Error>;
    fetchUserByUserName(userName: string): Promise<User[] | Error>;
    fetchAllUsers(): Promise<User[] | Error>;
    fetchRolesByUser(id: string): Promise<Role[]>;
    fetchUsersByRoleID(id: string): Promise<UserMapping>;
    fetchSubProjectsByProjectID(projectId: string): Promise<SubProject>;
    fetchAllRoles(): Promise<Role[]>;
    fetchAllUserMappings(): Promise<UserMapping[] | Error>;
    createRole(reqBody: string): Promise<number>;
    updateRoleByID(reqBody: string, id: string): Promise<number>;
    createUserMapping(reqBody: string): Promise<number>;
    deleteUserByID(id: string): Promise<number>;
    deleteRoleById(id: string): Promise<number>;
    deleteUserMappingByUserId(id: string): Promise<number>;
    deleteUserMappingByRoleId(id: string): Promise<number>;
    deleteUserMapping(id: string): Promise<number>;
}
//# sourceMappingURL=user.provider.interface.d.ts.map