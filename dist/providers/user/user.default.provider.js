"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_1 = __importDefault(require("http"));
const info_commons_1 = require("info-commons");
const knex_config_1 = require("knex-config");
const querystring_1 = __importDefault(require("querystring"));
const rename_keys_1 = __importDefault(require("rename-keys"));
const config_1 = __importDefault(require("../../config"));
const constants_1 = require("../../constants");
const models_1 = require("../../models");
const knex = knex_config_1.knexClient(info_commons_1.config.get(constants_1.DB), constants_1.METADB, info_commons_1.DIALECT);
class UserDefaultProvider {
    async createUser(reqBody) {
        const userData = this.getRenamedObject(reqBody);
        const result = await knex('USERDETAIL').insert(userData);
        if (result !== null && result.length > 0) {
            return result[0];
        }
        else {
            return 0;
        }
    }
    async updateUserStatus(userName, status) {
        const result = await knex(String(config_1.default.get(constants_1.AUTH_TABLE_KEY))).update('USER_STATUS', status)
            .where('USERNAME', userName).orWhere('EMAIL_ADDRESS', userName);
        return result;
    }
    async updateUserByID(reqBody, id) {
        const userData = this.getRenamedObject(reqBody);
        userData.UPDATED_AT = new Date();
        return await knex('USERDETAIL').update(userData).where('USERID', '=', id);
    }
    async updateAllUsers(reqBody) {
        const data = this.getRenamedObject(reqBody);
        data.UPDATED_AT = new Date();
        return knex('USERDETAIL').update(data)
            .then((result) => {
            return result;
        })
            .catch((err) => {
            throw err;
        });
    }
    // Defining PostMail function to call info-email-service
    async postMail(data, token, res) {
        // Build the body string from an object
        const body = querystring_1.default.stringify({
            to: `${data}`,
            cc: '',
            bcc: '',
            subject: 'DashBoardPoint:Password Reset Request',
            message: `Please follow the link to reset your password. http://localhost:3000/resetPassword/${token}`,
        });
        // An object of options to indicate where to post to
        const postOptions = {
            host: config_1.default.get('emailHost'),
            port: config_1.default.get('emailPort'),
            path: '/v1/sendMail',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json',
                'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NDg0MTk1MzV9.' +
                    'U4-whVXdG0-OqbEMD0DSz0gJFCUha42x-YW4n3iYHe0',
            },
        };
        /* Set up the request*/
        const postReq = await http_1.default.request(postOptions, (response) => {
            response.setEncoding('utf8');
            response.on('data', (chunk) => {
                if (response.statusCode === 200) {
                    return res.status(200).json({
                        message: 'Reset Password details are sent to' +
                            'your email.', response: chunk,
                    });
                }
                else {
                    return res.status(response.statusCode).json(chunk);
                }
            });
        }).on('error', (e) => {
            res.status(500).json({ message: 'Unable to send Email', error: `${e.message}` });
        });
        // post data
        postReq.write(body);
        postReq.end();
        return;
    }
    // function to create return object for postmail service
    postMailReturnObject(statusCode, chunk) {
        const mapObj = {
            status: statusCode,
            responseChunk: chunk,
        };
        return mapObj;
    }
    async forgotPassword(id) {
        return knex('USERDETAIL').where('USERID', '=', id).orWhere('EMAILADDRESS', '=', id)
            .then((result) => {
            return result;
        })
            .catch((err) => {
            throw err;
        });
    }
    async fetchUserByID(id) {
        return models_1.User.find(id);
    }
    async fetchSubProjectsByProjectID(projectID) {
        return models_1.SubProject.find(projectID);
    }
    async fetchUserByUserName(userName) {
        return knex('USERDETAIL').where('USERNAME', '=', userName)
            .then((result) => {
            return result;
        })
            .catch((err) => {
            throw err;
        });
    }
    async fetchAllUsers() {
        return models_1.User.findAll();
    }
    async fetchRolesByUser(id) {
        return models_1.Role.find(id);
    }
    async fetchUsersByRoleID(id) {
        return models_1.UserMapping.find(id);
    }
    async fetchAllRoles() {
        return models_1.Role.findAll();
    }
    async createRole(reqBody) {
        return knex('ROLE').insert(this.getRoleRenamedObject(reqBody))
            .then((result) => {
            return result[0];
        })
            .catch((err) => {
            throw err;
        });
    }
    async updateRoleByID(reqBody, id) {
        return knex('ROLE').update(this.getRoleRenamedObject(reqBody)).where('ROLEID', '=', id)
            .then((result) => {
            return result;
        })
            .catch((err) => {
            throw err;
        });
    }
    async fetchAllUserMappings() {
        return models_1.UserMapping.findAll();
    }
    async createUserMapping(reqBody) {
        return knex('USERMAPPING').insert(this.getUserMappingRenamedObject(reqBody))
            .then((result) => {
            return result[0];
        })
            .catch((err) => {
            throw err;
        });
    }
    async deleteUserByID(id) {
        return knex('USERDETAIL').where('USERID', '=', id).del()
            .then((result) => {
            return result;
        })
            .catch((err) => {
            throw err;
        });
    }
    async deleteRoleById(id) {
        return knex('ROLE').where('ROLEID', '=', id).del()
            .then((result) => {
            return result;
        })
            .catch((err) => {
            throw err;
        });
    }
    async deleteUserMappingByUserId(id) {
        return knex('USERMAPPING').where('USERID', '=', id).del()
            .then((result) => {
            return result;
        })
            .catch((err) => {
            throw err;
        });
    }
    async deleteUserMappingByRoleId(id) {
        return knex('USERMAPPING').where('ROLEID', '=', id).del()
            .then((result) => {
            return result;
        })
            .catch((err) => {
            throw err;
        });
    }
    async deleteUserMapping(id) {
        return knex('USERMAPPING').where('USERMAPPINGID', '=', id).del()
            .then((result) => {
            return result;
        })
            .catch((err) => {
            throw err;
        });
    }
    // Function to rename keys in request body as per UserDetail table
    getRenamedObject(reqbody) {
        const mapObj = {
            userName: 'USERNAME', password: 'PASSWORD', firstName: 'FIRSTNAME',
            lastName: 'LASTNAME', emailAddress: 'EMAIL_ADDRESS', verificationKey: 'VERIFICATION_KEY',
            userLoginId: 'USER_LOGIN_ID', userKey: 'USER_KEY', contactNumber: 'CONTACTNUMBER',
            profileStatus: 'PROFILESTATUS', photo: 'PHOTO', mobileNumber: 'MOBILE_NUMBER',
            dateOfBirth: 'DATE_OF_BIRTH', gender: 'GENDER', caste: 'CASTE',
            officePhoneNumber: 'OFFICE_PHONE_NUMBER', qualification: 'QUALIFICATION',
            middleName: 'MIDDLE_NAME', positionTitle: 'POSITION_TITLE',
            companyEmailAddress: 'COMPANY_EMAIL_ADDREES', companyLinkedInID: 'COMPANY_LINKEDIN_ID',
            passwordChanged: 'PASSWORD_CHANGED', passwordStatus: 'PASSWORD_STATUS',
        };
        const map = Object(mapObj);
        const obj = rename_keys_1.default(reqbody, (key, val) => {
            return map[key];
        });
        return obj;
    }
    getRoleRenamedObject(reqBody) {
        const mapObj = { roleName: 'ROLENAME' };
        const map = Object(mapObj);
        const obj = rename_keys_1.default(reqBody, (key, val) => {
            return map[key];
        });
        return obj;
    }
    getUserMappingRenamedObject(reqBody) {
        const mapObj = {
            userMappingId: 'USERMAPPINGID', userId: 'USERID',
            projectId: 'PROJECTID', roleId: 'ROLEID', itemId: 'ITEMID',
            mappingType: 'MAPPINGTYPE', status: 'STATUS', defaultFlag: 'DEFAULTFLAG',
        };
        const map = Object(mapObj);
        const obj = rename_keys_1.default(reqBody, (key, val) => {
            return map[key];
        });
        return obj;
    }
}
exports.default = new UserDefaultProvider();
//# sourceMappingURL=user.default.provider.js.map