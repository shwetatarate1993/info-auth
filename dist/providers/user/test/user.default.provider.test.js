"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon_1 = __importDefault(require("sinon"));
const user_sqlitedb_1 = require("../../../mockdata/user.sqlitedb");
const providers_1 = require("../../../providers");
beforeAll(async () => {
    await user_sqlitedb_1.createUserDetailTable();
});
beforeEach(async () => {
    await user_sqlitedb_1.seedUserDetailTable();
});
afterEach(async () => {
    await user_sqlitedb_1.truncateUserDetailTable();
});
describe('UPDATE USER STATUS', async () => {
    test.skip('Update User Status for non-existing user - Failure', async () => {
        const result = await providers_1.userDefaultProvider.updateUserStatus('abc', 'ACTIVE');
        sinon_1.default.assert.match(result, 0);
    });
    test.skip('Update User Status for existing user - Success', async () => {
        // Assertion before user status update
        // const user: UserDetail = await authDefaultProvider.fetchUserByEmailID('Ankur');
        // sinon.assert.match(user.userStatus, 'PENDING_VERIFICATION');
        // await userDefaultProvider.updateUserStatus('Ankur', 'ACTIVE');
        // // Assertion after user status update
        // const userAfterUpdate: UserDetail = await authDefaultProvider.fetchUserByEmailID('Ankur');
        // sinon.assert.match(userAfterUpdate.userStatus, 'ACTIVE');
    });
});
//# sourceMappingURL=user.default.provider.test.js.map