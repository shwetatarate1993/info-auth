import { Response } from 'express';
import { Role, SubProject, User, UserMapping } from '../../models';
import UserProvider from './user.provider.interface';
declare class UserDefaultProvider implements UserProvider {
    createUser(reqBody: any): Promise<number>;
    updateUserStatus(userName: string, status: string): Promise<number>;
    updateUserByID(reqBody: string, id: string): Promise<number>;
    updateAllUsers(reqBody: string): Promise<number>;
    postMail(data: string, token: string, res: Response): Promise<Response>;
    postMailReturnObject(statusCode: number, chunk: string): object;
    forgotPassword(id: string): Promise<User[]>;
    fetchUserByID(id: number): Promise<User[] | Error>;
    fetchSubProjectsByProjectID(projectID: string): Promise<SubProject[]>;
    fetchUserByUserName(userName: string): Promise<User[]>;
    fetchAllUsers(): Promise<User[] | Error>;
    fetchRolesByUser(id: string): Promise<Role[]>;
    fetchUsersByRoleID(id: string): Promise<UserMapping>;
    fetchAllRoles(): Promise<Role[]>;
    createRole(reqBody: string): Promise<number>;
    updateRoleByID(reqBody: string, id: string): Promise<number>;
    fetchAllUserMappings(): Promise<UserMapping[] | Error>;
    createUserMapping(reqBody: string): Promise<number>;
    deleteUserByID(id: string): Promise<number>;
    deleteRoleById(id: string): Promise<number>;
    deleteUserMappingByUserId(id: string): Promise<number>;
    deleteUserMappingByRoleId(id: string): Promise<number>;
    deleteUserMapping(id: string): Promise<number>;
    getRenamedObject(reqbody: string): any;
    getRoleRenamedObject(reqBody: string): any;
    getUserMappingRenamedObject(reqBody: string): any;
}
declare const _default: UserDefaultProvider;
export default _default;
//# sourceMappingURL=user.default.provider.d.ts.map