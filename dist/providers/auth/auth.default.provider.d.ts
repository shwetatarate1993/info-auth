import Knex from 'knex';
import { UserDetail } from '../../models';
import AuthProvider from './auth.provider.interface';
declare class AuthDefaultProvider implements AuthProvider {
    knex: Knex;
    constructor(configDB: any, databaseId: string, dialect: string);
    authentication(userName: string, password: string, relayState: string): Promise<any>;
    updateUserStatus(userName: string, status: string): Promise<number>;
    fetchUserDetail(emailID: string, password: string): Promise<UserDetail>;
    fetchUserByEmailID(emailID: string): Promise<UserDetail>;
    resetPassword(emailID: string, newPassword: string): Promise<number>;
    updateVerificationKey(userName: string, code: string): Promise<number>;
    incrementLoginAttempt(emailID: string): Promise<number>;
    createUser(reqBody: any): Promise<number>;
    resetLoginAttempt(emailID: string): Promise<number>;
    updateUser(fields: object, emailID: string): Promise<number>;
    getRenamedObject(reqbody: string): any;
}
declare const getAuthDefaultProvider: (configDB: any, databaseId: string, dialect: string) => AuthDefaultProvider;
export default getAuthDefaultProvider;
//# sourceMappingURL=auth.default.provider.d.ts.map