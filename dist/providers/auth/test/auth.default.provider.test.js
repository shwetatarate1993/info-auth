"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const sinon_1 = __importDefault(require("sinon"));
const constants_1 = require("../../../constants");
const user_sqlitedb_1 = require("../../../mockdata/user.sqlitedb");
const providers_1 = require("../../../providers");
const authDefaultProvider = providers_1.getAuthDefaultProvider(info_commons_1.config.get(constants_1.DB), constants_1.MOCK, 'sqlite3');
const pwdEncrypted = 'e534a11775e3e9416e0e45a2193960f4$e6db998ac053572a0' +
    'e0dca0e62d6af076513f717def07cdddf96ba3680a85df6';
beforeAll(async () => {
    await user_sqlitedb_1.createUserDetailTable();
    await user_sqlitedb_1.createUserAuthTable();
});
beforeEach(async () => {
    await user_sqlitedb_1.seedUserDetailTable();
    await user_sqlitedb_1.seedUserAuthTable();
});
afterEach(async () => {
    await user_sqlitedb_1.truncateUserDetailTable();
    await user_sqlitedb_1.truncateUserAuthTable();
});
describe('FETCH USER DETAIL', () => {
    test('User Not Found', async () => {
        const user = await authDefaultProvider.fetchUserDetail('abc', 'pwd');
        sinon_1.default.assert.match(user, null);
    });
    test('Fetch User By UserName & Password - User Found', async () => {
        const user = await authDefaultProvider.fetchUserDetail('Ankur', pwdEncrypted);
        sinon_1.default.assert.match(user.email, 'guptaankur798@gmail.com');
    });
    test('Fetch User By Email & Password - User Found', async () => {
        const user = await authDefaultProvider.fetchUserDetail('guptaankur798@gmail.com', pwdEncrypted);
        sinon_1.default.assert.match(user.email, 'guptaankur798@gmail.com');
    });
});
describe('FETCH USER BY USERNAME', () => {
    test('User Not Found', async () => {
        const user = await authDefaultProvider.fetchUserByEmailID('abc');
        sinon_1.default.assert.match(user, null);
    });
    test('Fetch User By UserName - User Found', async () => {
        const user = await authDefaultProvider.fetchUserByEmailID('Ankur');
        sinon_1.default.assert.match(user.email, 'guptaankur798@gmail.com');
    });
    test('Fetch User By Email - User Found', async () => {
        const user = await authDefaultProvider.fetchUserByEmailID('guptaankur798@gmail.com');
        sinon_1.default.assert.match(user.email, 'guptaankur798@gmail.com');
    });
});
describe('RESET PASSWORD', () => {
    test('Reset Password for non-existing user - Failure', async () => {
        const result = await authDefaultProvider.resetPassword('abc', 'newPass');
        sinon_1.default.assert.match(result, 0);
    });
    test('Reset Password for existing user - Success', async () => {
        // Assertion before password reset
        const user = await authDefaultProvider.fetchUserByEmailID('Ankur');
        sinon_1.default.assert.match(user.password, pwdEncrypted);
        await authDefaultProvider.resetPassword('Ankur', 'newPass');
        // Assertion after password reset
        const userAfterPassReset = await authDefaultProvider.fetchUserByEmailID('Ankur');
        sinon_1.default.assert.match(userAfterPassReset.password, 'newPass');
    });
});
describe('UPDATE VERIFICATION KEY(code)', () => {
    test('Update Verification Key for non-existing user - Failure', async () => {
        const result = await authDefaultProvider.resetPassword('abc', 'newCode');
        sinon_1.default.assert.match(result, 0);
    });
    test('Update Verification Key for existing user - Success', async () => {
        // Assertion before code update
        const user = await authDefaultProvider.fetchUserByEmailID('Ankur');
        sinon_1.default.assert.match(user.code, 'abc');
        await authDefaultProvider.updateVerificationKey('Ankur', 'newCode');
        // Assertion after code update
        const userAfterCodeUpdate = await authDefaultProvider.fetchUserByEmailID('Ankur');
        sinon_1.default.assert.match(userAfterCodeUpdate.code, 'newCode');
    });
});
describe('INCREMENT LOGIN ATTEMPT', () => {
    test('Increment Login Attempt for non-existing user - Failure', async () => {
        const result = await authDefaultProvider.incrementLoginAttempt('abc');
        sinon_1.default.assert.match(result, 0);
    });
    test('Increment Login Attemp for existing user - Success', async () => {
        // Assertion before code update
        const user = await authDefaultProvider.fetchUserByEmailID('Ankur');
        sinon_1.default.assert.match(user.loginAttempt, 0);
        await authDefaultProvider.incrementLoginAttempt('Ankur');
        // Assertion after code update
        const userAfterIncrement = await authDefaultProvider.fetchUserByEmailID('Ankur');
        sinon_1.default.assert.match(userAfterIncrement.loginAttempt, 1);
    });
});
describe('RESET LOGIN ATTEMPT', () => {
    test('Reset Login Attempt for non-existing user - Failure', async () => {
        const result = await authDefaultProvider.resetLoginAttempt('abc');
        sinon_1.default.assert.match(result, 0);
    });
    test('Reset Login Attemp for existing user - Success', async () => {
        // Incrementing Login Attempt for twice for the same user
        await authDefaultProvider.incrementLoginAttempt('Ankur');
        await authDefaultProvider.incrementLoginAttempt('guptaankur798@gmail.com');
        // Assertion before LoginAttempt reset
        const user = await authDefaultProvider.fetchUserByEmailID('Ankur');
        sinon_1.default.assert.match(user.loginAttempt, 2);
        await authDefaultProvider.resetLoginAttempt('Ankur');
        // Assertion after LoginAttempt reset
        const userAfterReset = await authDefaultProvider.fetchUserByEmailID('Ankur');
        sinon_1.default.assert.match(userAfterReset.loginAttempt, 0);
    });
});
//# sourceMappingURL=auth.default.provider.test.js.map