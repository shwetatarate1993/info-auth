"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const jwt = __importStar(require("jsonwebtoken"));
const knex_config_1 = require("knex-config");
const rename_keys_1 = __importDefault(require("rename-keys"));
const config_1 = __importDefault(require("../../config"));
const constants_1 = require("../../constants");
const models_1 = require("../../models");
class AuthDefaultProvider {
    constructor(configDB, databaseId, dialect) {
        this.knex = knex_config_1.knexClient(configDB, databaseId, dialect);
        models_1.UserAuth.knex = this.knex;
    }
    async authentication(userName, password, relayState) {
        const usr = await models_1.UserAuth.fetchUser(userName, password);
        let user = Object(usr);
        if (user && user.length) {
            let response;
            user = user[0];
            // creating jwt token based on UserID and Secret key
            const token = jwt.sign({
                userId: user.userId, userName: user.userName,
                EMAIL_ADDRESS: user.EMAIL_ADDRESS,
            }, config_1.default.get('secret'), { expiresIn: 1800 });
            if ('PASSWORD_EXPIRED' === user.passwordStatus) {
                response = {
                    statusToken: token, expiresAt: user.expiresAt,
                    status: 'PASSWORD_EXPIRED', relayState, user: {
                        id: user.userId,
                        passwordChanged: user.passwordChanged, profile: user,
                    },
                    links: {
                        changePassword: {
                            href: config_1.default.get('links').change_password,
                            hints: { allow: ['POST'] },
                        }, cancel: {
                            href: config_1.default.get('links').cancel,
                            hints: { allow: ['POST'] },
                        },
                    },
                };
                return response;
            }
            if ('LOCKED_OUT' === user.passwordStatus) {
                response = {
                    status: 'LOCKED_OUT', links: {
                        unlock: { href: config_1.default.get('links').unlock_password, hints: { allow: ['POST'] } },
                    },
                };
                return response;
            }
            response = {
                expiresAt: user.expiresAt, status: 'SUCCESS', relayState,
                accessToken: token, user: {
                    id: user.userId,
                    passwordChanged: user.passwordChanged, profile: user,
                },
            };
            return response;
        }
        else {
            return models_1.UserAuth.fetchUserByEmailAddress(userName)
                .then((userData) => {
                const error = new info_commons_1.ErrorResponse();
                const errors = [];
                error.code = 401;
                if (userData && Object.keys(userData).length !== 0) {
                    error.message = 'You have entered invalid password';
                    errors.push({
                        domain: 'global', reason: 'Incorrect Password',
                        message: 'Please enter correct password', locationType: 'Parameter',
                        location: 'password', extendedHelp: '' + config_1.default.get('links').extendedHelp,
                    });
                }
                else {
                    error.message = 'You have entered invalid Email address';
                    errors.push({
                        domain: 'global', reason: 'Incorrect Email address',
                        message: 'Please enter correct email address', locationType: 'Parameter',
                        location: 'email', extendedHelp: '' + config_1.default.get('links').extendedHelp,
                    });
                }
                error.errors = errors;
                return { error };
            })
                .catch((err) => {
                throw err;
            });
        }
    }
    async updateUserStatus(userName, status) {
        const result = await this.knex(String(config_1.default.get(constants_1.AUTH_TABLE_KEY))).update('USER_STATUS', status)
            .where('USERNAME', userName).orWhere('EMAIL_ADDRESS', userName);
        return result;
    }
    async fetchUserDetail(emailID, password) {
        const user = await this.knex(String(config_1.default.get(constants_1.AUTH_TABLE_KEY))).select(constants_1.USER_AUTH_FIELDS)
            .where({ USERNAME: emailID, PASSWORD: password }).orWhere({ EMAIL_ADDRESS: emailID, PASSWORD: password });
        if (user.length > 0) {
            return user[0];
        }
        return null;
    }
    async fetchUserByEmailID(emailID) {
        const user = await this.knex(String(config_1.default.get(constants_1.AUTH_TABLE_KEY))).select(constants_1.USER_AUTH_FIELDS)
            .where('USERNAME', emailID).orWhere('EMAIL_ADDRESS', emailID);
        if (user.length > 0) {
            return user[0];
        }
        return null;
    }
    async resetPassword(emailID, newPassword) {
        const result = await this.knex(String(config_1.default.get(constants_1.AUTH_TABLE_KEY)))
            .update({ PASSWORD: newPassword, USER_STATUS: 'ACTIVE' })
            .where('USERNAME', emailID).orWhere('EMAIL_ADDRESS', emailID);
        return result;
    }
    async updateVerificationKey(userName, code) {
        const result = await this.knex(String(config_1.default.get(constants_1.AUTH_TABLE_KEY))).update('VERIFICATION_KEY', code)
            .where('USERNAME', userName).orWhere('EMAIL_ADDRESS', userName);
        return result;
    }
    async incrementLoginAttempt(emailID) {
        const result = await this.knex(String(config_1.default.get(constants_1.AUTH_TABLE_KEY))).increment('LOGIN_ATTEMPT')
            .where('USERNAME', emailID).orWhere('EMAIL_ADDRESS', emailID);
        return result;
    }
    async createUser(reqBody) {
        const userData = this.getRenamedObject(reqBody);
        const result = await this.knex(String(config_1.default.get(constants_1.AUTH_TABLE_KEY))).insert(userData);
        console.log(JSON.stringify(result));
        if (result !== null && result.length > 0) {
            console.log(JSON.stringify(result));
            return result.length;
        }
        else {
            return 0;
        }
    }
    async resetLoginAttempt(emailID) {
        const result = await this.knex(String(config_1.default.get(constants_1.AUTH_TABLE_KEY))).update('LOGIN_ATTEMPT', 0)
            .where('USERNAME', emailID).orWhere('EMAIL_ADDRESS', emailID);
        return result;
    }
    async updateUser(fields, emailID) {
        const result = await this.knex(String(config_1.default.get(constants_1.AUTH_TABLE_KEY))).update(fields)
            .where('USERNAME', emailID).orWhere('EMAIL_ADDRESS', emailID);
        return result;
    }
    getRenamedObject(reqbody) {
        const mapObj = {
            password: 'PASSWORD', email: 'EMAIL_ADDRESS', verificationKey: 'VERIFICATION_KEY',
        };
        const map = Object(mapObj);
        const obj = rename_keys_1.default(reqbody, (key, val) => {
            return map[key];
        });
        return obj;
    }
}
const getAuthDefaultProvider = (configDB, databaseId, dialect) => {
    return new AuthDefaultProvider(configDB, databaseId, dialect);
};
exports.default = getAuthDefaultProvider;
//# sourceMappingURL=auth.default.provider.js.map