import { UserDetail } from '../../models';
export default interface AuthProvider {
    authentication(emailID: string, password: string, relayState: string): Promise<any>;
    fetchUserDetail(userId: string, password: string): Promise<UserDetail>;
    fetchUserByEmailID(emailID: string): Promise<UserDetail>;
    resetPassword(emailID: string, newPassword: string): Promise<number>;
    updateVerificationKey(emailID: string, code: string): Promise<number>;
    incrementLoginAttempt(emailID: string): Promise<number>;
    resetLoginAttempt(emailID: string): Promise<number>;
    updateUser(fields: object, emailID: string): Promise<number>;
    createUser(reqBody: any): Promise<number>;
    updateUserStatus(userName: string, status: string): Promise<number>;
}
//# sourceMappingURL=auth.provider.interface.d.ts.map