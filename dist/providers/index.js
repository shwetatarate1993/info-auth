"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const config_1 = __importDefault(require("../config"));
const constants_1 = require("../constants");
var auth_default_provider_1 = require("../providers/auth/auth.default.provider");
exports.getAuthDefaultProvider = auth_default_provider_1.default;
var user_default_provider_1 = require("../providers/user/user.default.provider");
exports.userDefaultProvider = user_default_provider_1.default;
const _1 = require("./");
exports.getAuthProvider = (configDB, databaseId, dialect) => {
    switch (config_1.default.get(info_commons_1.PROVIDER)) {
        case constants_1.DEFAULT:
            return _1.getAuthDefaultProvider(configDB, databaseId, dialect);
        default:
            throw new Error('Authentication Provider not found for provider ' + config_1.default.get(info_commons_1.PROVIDER));
    }
};
exports.getUserProvider = () => {
    switch (config_1.default.get(info_commons_1.PROVIDER)) {
        case constants_1.DEFAULT:
            return _1.userDefaultProvider;
        default:
            throw new Error('User Provider not found for provider ' + config_1.default.get(info_commons_1.PROVIDER));
    }
};
//# sourceMappingURL=index.js.map