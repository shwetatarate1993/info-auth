export declare enum ResponseTypeEnum {
    ID_TOKEN = "id_token",
    TOKEN = "token"
}
export declare const parseResponseTypeEnum: (responseType: string) => ResponseTypeEnum;
//# sourceMappingURL=responsetype.enum.d.ts.map