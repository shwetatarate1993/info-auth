export declare enum ScopeEnum {
    OPEN_ID = "openid",
    EMAIL = "email",
    PHONE = "phone",
    PROFILE = "profile",
    ADDRESS = "address"
}
export declare const parseScopeEnum: (scope: string) => ScopeEnum;
//# sourceMappingURL=scope.enum.d.ts.map