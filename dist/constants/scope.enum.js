"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ScopeEnum;
(function (ScopeEnum) {
    ScopeEnum["OPEN_ID"] = "openid";
    ScopeEnum["EMAIL"] = "email";
    ScopeEnum["PHONE"] = "phone";
    ScopeEnum["PROFILE"] = "profile";
    ScopeEnum["ADDRESS"] = "address";
})(ScopeEnum = exports.ScopeEnum || (exports.ScopeEnum = {}));
exports.parseScopeEnum = (scope) => {
    switch (scope) {
        case ScopeEnum.OPEN_ID:
            return ScopeEnum.OPEN_ID;
        case ScopeEnum.EMAIL:
            return ScopeEnum.EMAIL;
        case ScopeEnum.PHONE:
            return ScopeEnum.PHONE;
        case ScopeEnum.PROFILE:
            return ScopeEnum.PROFILE;
        case ScopeEnum.ADDRESS:
            return ScopeEnum.ADDRESS;
        default:
            return null;
    }
};
//# sourceMappingURL=scope.enum.js.map