"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DEFAULT = 'default';
exports.DB = 'db';
exports.MOCK = 'mock';
exports.METADB = 'METADB';
exports.AUTH_TABLE_KEY = 'authTable';
exports.ID_TOKEN = 'id_token';
exports.TOKEN = 'token';
exports.REFRESH_TOKEN_KEY = 'refresh-token';
var responsetype_enum_1 = require("./responsetype.enum");
exports.ResponseTypeEnum = responsetype_enum_1.ResponseTypeEnum;
exports.parseResponseTypeEnum = responsetype_enum_1.parseResponseTypeEnum;
var scope_enum_1 = require("./scope.enum");
exports.ScopeEnum = scope_enum_1.ScopeEnum;
exports.parseScopeEnum = scope_enum_1.parseScopeEnum;
var error_reason_enum_1 = require("./error.reason.enum");
exports.ErrorReasonEnum = error_reason_enum_1.ErrorReasonEnum;
exports.SPACE = ' ';
exports.EMAIL = 'email';
exports.PHONE = 'phone';
exports.ADDRESS = 'address';
exports.PROFILE = 'profile';
var dbengine_enum_1 = require("./dbengine.enum");
exports.DBEngine = dbengine_enum_1.DBEngine;
exports.parseDBEngineEnum = dbengine_enum_1.parseDBEngineEnum;
exports.USER_FIELDS = ['USERID as userId', 'PASSWORD as password',
    'USERNAME as name', 'FIRSTNAME as givenName',
    'LASTNAME as familyName', 'USERSTATUS as userStatus', 'MIDDLE_NAME as middleName',
    'EMAILADDRESS as email', 'MOBILE_NUMBER as phoneNumber', 'PHOTO_URL as picture',
    'DATE_OF_BIRTH as birthDate', 'GENDER as gender', 'EMAIL_VERIFIED as emailVerified',
    'PHONE_NUMBER_VERIFIED as phoneNumberVerified', 'ADDRESS as address',
    'NICK_NAME as nickName', 'PREFERRED_USERNAME as preferredUserName',
    'PROFILE as profile', 'WEBSITE as website', 'ZONE_INFO as zoneInfo', 'LOCALE as locale',
    'CREATED_AT as createdAt', 'UPDATED_AT as updatedAt', 'VERIFICATIONKEY as code',
    'LOGIN_ATTEMPT as loginAttempt', 'REFRESH_TOKEN as refreshToken'];
exports.USER_AUTH_FIELDS = ['USER_AUTH_ID as userAuthId',
    'PASSWORD as password', 'USER_STATUS as userStatus',
    'EMAIL_ADDRESS as email', 'VERIFICATION_KEY as code', 'LOGIN_ATTEMPT as loginAttempt'];
//# sourceMappingURL=index.js.map