"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ErrorReasonEnum;
(function (ErrorReasonEnum) {
    ErrorReasonEnum["MISSING_PARAMETER"] = "MISSING_PARAMETER";
    ErrorReasonEnum["INVALID_PARAMETER"] = "INVALID_PARAMETER";
    ErrorReasonEnum["INVALID_USERNAME"] = "INVALID_USERNAME";
    ErrorReasonEnum["MISSING_PARAMETER_VALUE"] = "MISSING_PARAMETER_VALUE";
    ErrorReasonEnum["PENDING_VERIFICATION"] = "PENDING_CODE_VERIFICATION";
    ErrorReasonEnum["INVALID_PWD_RESET_CODE"] = "INVALID_PASSWORD_RESET_CODE";
    ErrorReasonEnum["INVALID_OLD_PWD"] = "INVALID_OLD_PASSWORD";
    ErrorReasonEnum["INVALID_CODE"] = "INVALID_CODE";
    ErrorReasonEnum["INVALID_TOKEN"] = "INVALID_TOKEN";
    ErrorReasonEnum["DEACTIVATED"] = "DEACTIVATED";
    ErrorReasonEnum["MISSING_TOKEN"] = "MISSING_TOKEN";
    ErrorReasonEnum["PENDING_CHANGE_PASSWORD"] = "PENDING_CHANGE_PASSWORD";
    ErrorReasonEnum["LOCKED"] = "LOCKED";
    ErrorReasonEnum["INVALID_EMAIL_FORMAT"] = "INVALID_EMAIL_FORMAT";
    ErrorReasonEnum["WEAK_PASSWORD"] = "WEAK_PASSWORD";
    ErrorReasonEnum["INVALID_CREDENTIAL"] = "INVALID_CREDENTIAL";
    ErrorReasonEnum["TOKEN_MISMATCH"] = "TOKEN_MISMATCH";
    ErrorReasonEnum["INVALID_PROVIDER"] = "INVALID_PROVIDER";
})(ErrorReasonEnum = exports.ErrorReasonEnum || (exports.ErrorReasonEnum = {}));
//# sourceMappingURL=error.reason.enum.js.map