"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ResponseTypeEnum;
(function (ResponseTypeEnum) {
    ResponseTypeEnum["ID_TOKEN"] = "id_token";
    ResponseTypeEnum["TOKEN"] = "token";
})(ResponseTypeEnum = exports.ResponseTypeEnum || (exports.ResponseTypeEnum = {}));
exports.parseResponseTypeEnum = (responseType) => {
    switch (responseType) {
        case ResponseTypeEnum.ID_TOKEN:
            return ResponseTypeEnum.ID_TOKEN;
        case ResponseTypeEnum.TOKEN:
            return ResponseTypeEnum.TOKEN;
        default:
            return null;
    }
};
//# sourceMappingURL=responsetype.enum.js.map