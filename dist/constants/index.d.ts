export declare const DEFAULT: string;
export declare const DB: string;
export declare const MOCK: string;
export declare const METADB: string;
export declare const AUTH_TABLE_KEY: string;
export declare const ID_TOKEN: string;
export declare const TOKEN: string;
export declare const REFRESH_TOKEN_KEY: string;
export { ResponseTypeEnum, parseResponseTypeEnum } from './responsetype.enum';
export { ScopeEnum, parseScopeEnum } from './scope.enum';
export { ErrorReasonEnum } from './error.reason.enum';
export declare const SPACE: string;
export declare const EMAIL: string;
export declare const PHONE: string;
export declare const ADDRESS: string;
export declare const PROFILE: string;
export { DBEngine, parseDBEngineEnum } from './dbengine.enum';
export declare const USER_FIELDS: string[];
export declare const USER_AUTH_FIELDS: string[];
//# sourceMappingURL=index.d.ts.map