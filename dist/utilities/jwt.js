"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_jwt_1 = __importDefault(require("express-jwt"));
const config_1 = __importDefault(require("../config"));
module.exports = jwt;
function jwt() {
    const secret = config_1.default.get('secret');
    return express_jwt_1.default({ secret }).unless({
        path: [
            // public routes that don't require authentication
            '/user/authenticate',
        ],
    });
}
//# sourceMappingURL=jwt.js.map