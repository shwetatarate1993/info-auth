"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_1 = __importDefault(require("http"));
const querystring_1 = __importDefault(require("querystring"));
const config_1 = __importDefault(require("../config"));
// Defining PostMail function to call info-email-service
exports.postMail = (emailId, subject, message) => {
    // Build the body string from an object
    const body = querystring_1.default.stringify({
        to: emailId,
        cc: '',
        bcc: '',
        subject,
        message,
    });
    // An object of options to indicate where to post to
    const postOptions = {
        host: config_1.default.get('emailHost'),
        port: config_1.default.get('emailPort'),
        path: '/v1/sendMail',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json',
        },
    };
    // Set up request & postdata
    const postReq = http_1.default.request(postOptions);
    postReq.write(body);
    postReq.end();
};
//# sourceMappingURL=mailing.js.map