"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const crypto_1 = __importDefault(require("crypto"));
exports.keyEncryption = (key) => {
    const salt = crypto_1.default.randomBytes(16).toString('hex');
    const hash = crypto_1.default.pbkdf2Sync(key, salt, 2048, 32, 'sha512').toString('hex');
    const encryptedKey = [salt, hash].join('$');
    return encryptedKey;
};
exports.verifyingKey = (encrptedKey, key) => {
    const salt = encrptedKey.split('$')[0];
    const hash = crypto_1.default.pbkdf2Sync(key, salt, 2048, 32, 'sha512').toString('hex');
    const encrypData = [salt, hash].join('$');
    return encrypData;
};
//# sourceMappingURL=encryption.js.map