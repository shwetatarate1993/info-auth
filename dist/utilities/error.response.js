"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const constants_1 = require("../constants");
exports.invalidUserNameOrEmail = async () => {
    const error = new info_commons_1.ErrorResponse();
    error.code = 403;
    error.message = 'Invalid Username or Email';
    error.errors = [{ domain: 'global', reason: constants_1.ErrorReasonEnum.INVALID_USERNAME,
            message: 'Invalid Username or Email', locationType: 'Request Body',
            location: 'Username', extendedHelp: 'http://request/help' }];
    return error;
};
exports.invalidEmailFormat = async () => {
    const error = new info_commons_1.ErrorResponse();
    error.code = 422;
    error.message = 'Invalid Email Format';
    error.errors = [{ domain: 'global', reason: constants_1.ErrorReasonEnum.INVALID_EMAIL_FORMAT,
            message: 'Invalid Email Format', locationType: 'Request Body',
            location: 'Email', extendedHelp: 'http://request/help' }];
    return error;
};
exports.weakPassword = async () => {
    const error = new info_commons_1.ErrorResponse();
    error.code = 422;
    error.message = 'Weak Password';
    error.errors = [{ domain: 'global', reason: constants_1.ErrorReasonEnum.WEAK_PASSWORD,
            message: 'Password must contain minimum 8 and maximum 20 letters. It must contain atleast ' +
                'one uppercase & one lowercase character. It must contain atleast one digit. It must ' +
                'contain atleast one special symbol.',
            locationType: 'Request Body', location: 'Password',
            extendedHelp: 'http://request/help' }];
    return error;
};
exports.userAlreadyExist = async () => {
    const error = new info_commons_1.ErrorResponse();
    error.code = 422;
    error.message = 'User Already Exists';
    error.errors = [{ domain: 'global', reason: constants_1.ErrorReasonEnum.WEAK_PASSWORD,
            message: 'User Already Exists',
            locationType: 'Request Body', location: 'Password',
            extendedHelp: 'http://request/help' }];
    return error;
};
//# sourceMappingURL=error.response.js.map