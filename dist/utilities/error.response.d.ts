import { ErrorResponse } from 'info-commons';
export declare const invalidUserNameOrEmail: () => Promise<ErrorResponse>;
export declare const invalidEmailFormat: () => Promise<ErrorResponse>;
export declare const weakPassword: () => Promise<ErrorResponse>;
export declare const userAlreadyExist: () => Promise<ErrorResponse>;
//# sourceMappingURL=error.response.d.ts.map