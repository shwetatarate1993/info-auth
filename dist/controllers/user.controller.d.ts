import { NextFunction, Request, Response } from 'express';
declare class UserController {
    forgotPassword(req: Request, res: Response, next: NextFunction): Promise<import("express-serve-static-core").Response>;
    passwordReset(req: Request, res: Response, next: NextFunction): Promise<void>;
    createUser(req: Request, res: Response, next: NextFunction): Promise<import("express-serve-static-core").Response>;
    updateUserStatus(req: Request, res: Response): Promise<import("express-serve-static-core").Response>;
    updateUserByID(req: Request, res: Response, next: NextFunction): Promise<import("express-serve-static-core").Response>;
    updateAllUsers(req: Request, res: Response, next: NextFunction): Promise<import("express-serve-static-core").Response>;
    fetchUserByID(req: Request, res: Response, next: NextFunction): Promise<import("express-serve-static-core").Response>;
    fetchUserByUserName(req: Request, res: Response, next: NextFunction): Promise<import("express-serve-static-core").Response>;
    fetchAllUsers(req: Request, res: Response, next: NextFunction): Promise<import("express-serve-static-core").Response>;
    fetchRolesByUser(req: Request, res: Response, next: NextFunction): Promise<import("express-serve-static-core").Response>;
    fetchAllRoles(req: Request, res: Response, next: NextFunction): Promise<import("express-serve-static-core").Response>;
    createRole(req: Request, res: Response, next: NextFunction): Promise<import("express-serve-static-core").Response>;
    updateRoleByID(req: Request, res: Response, next: NextFunction): Promise<import("express-serve-static-core").Response>;
    fetchAllUserMappings(req: Request, res: Response, next: NextFunction): Promise<import("express-serve-static-core").Response>;
    createUserMapping(req: Request, res: Response, next: NextFunction): Promise<import("express-serve-static-core").Response>;
    deleteUserByID(req: Request, res: Response, next: NextFunction): Promise<import("express-serve-static-core").Response>;
    deleteRoleById(req: Request, res: Response, next: NextFunction): Promise<import("express-serve-static-core").Response>;
    deleteUserMapping(req: Request, res: Response, next: NextFunction): Promise<import("express-serve-static-core").Response>;
    fetchSubProjectByProjectID(req: Request, res: Response, next: NextFunction): Promise<import("express-serve-static-core").Response>;
    fetchUsersByRoleID(req: Request, res: Response, next: NextFunction): Promise<import("express-serve-static-core").Response>;
}
declare const _default: UserController;
export default _default;
//# sourceMappingURL=user.controller.d.ts.map