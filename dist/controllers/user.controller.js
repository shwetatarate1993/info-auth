"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const jwt = __importStar(require("jsonwebtoken"));
const v4_1 = __importDefault(require("uuid/v4"));
const isEmail_1 = __importDefault(require("validator/lib/isEmail"));
const config_1 = __importDefault(require("../config"));
const constants_1 = require("../constants");
const models_1 = require("../models");
const services_1 = require("../services");
const encryption_1 = require("../utilities/encryption");
const error_response_1 = require("../utilities/error.response");
const mailing_1 = require("../utilities/mailing");
class UserController {
    async forgotPassword(req, res, next) {
        try {
            // calling user service and fetching user data if present
            const mail = await services_1.userService.forgotPassword(req.params.id);
            if (Object.keys(mail).length !== 0) {
                const data = JSON.parse(JSON.stringify(mail[0]));
                const verikey = { verificationKey: v4_1.default() };
                // generating jwt token with payload(uuid and userid) and passing it in the email.
                const token = jwt.sign({ sub: data.USERID, jti: verikey.verificationKey }, config_1.default.get('secret'));
                // updating verification key of the user and if success Invoking PostMail function.
                services_1.userService.updateUserByID(verikey, data.USERID)
                    .then((result) => result === 1 ? services_1.userService.postMail(data.EMAILADDRESS, token, res) : res.status(500).json({ message: 'Unable to update verification key.' }))
                    .catch((err) => next(err));
            }
            else {
                return res.status(404).json({ message: 'User not found.' });
            }
        }
        catch (error) {
            return res.status(500).json({ message: 'Internal Server Error', response: error });
        }
    }
    async passwordReset(req, res, next) {
        jwt.verify(req.body.token, config_1.default.get('secret'), (err, decoded) => {
            if (!err) {
                const reset = async () => {
                    try {
                        const result = await services_1.userService.fetchUserByID(decoded.sub);
                        if (Object.keys(result).length !== 0) {
                            const data = JSON.parse(JSON.stringify(result[0]));
                            // matching the verification key and updating password
                            if (data.verificationKey === decoded.jti) {
                                services_1.userService.updateUserByID({
                                    password: req.body.newPass,
                                    verificationKey: 'null',
                                }, decoded.sub)
                                    .then((resData) => resData === 1 ?
                                    res.status(200).json({ message: 'Password updated successfully!!' }) :
                                    res.status(500).json({ message: 'Unable to update Password.' }))
                                    .catch((error) => next(error));
                            }
                            else {
                                return res.status(403).json({ message: 'Forbidden' });
                            }
                        }
                    }
                    catch (error) {
                        res.status(503).json({ message: 'Service Unavailable' });
                    }
                };
                reset();
            }
            else {
                return res.status(401).json({
                    message: 'Invalid token', error: {
                        type: err.name,
                        message: err.message,
                    },
                });
            }
        });
    }
    async createUser(req, res, next) {
        // Validate Mandatory Fields
        const mandatoryFieldsErrorResponse = await validateRequiredFields(['action',
            'userName', 'password', 'firstName', 'emailAddress'], req);
        if (mandatoryFieldsErrorResponse !== null) {
            return res.status(mandatoryFieldsErrorResponse.code).json(mandatoryFieldsErrorResponse);
        }
        if (!isEmail_1.default(req.body.emailAddress)) {
            const emailFormatError = await error_response_1.invalidEmailFormat();
            return res.status(emailFormatError.code).json(emailFormatError);
        }
        if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{8,20}$/.test(req.body.password)) {
            const weakPasswordError = await error_response_1.weakPassword();
            return res.status(weakPasswordError.code).json(weakPasswordError);
        }
        const userList = await services_1.userService.fetchUserByUserName(req.body.userName);
        if (userList.length > 0) {
            const userAlreadyExistError = await error_response_1.userAlreadyExist();
            return res.status(userAlreadyExistError.code).json(userAlreadyExistError);
        }
        const verificationCode = v4_1.default().substring(0, 8);
        const encrypVerificationKey = encryption_1.keyEncryption(verificationCode);
        req.body.verificationKey = encrypVerificationKey;
        const result = await services_1.userService.createUser(req.body, req.body.action);
        if (result > 0) {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Expose-Headers', 'location');
            res.header('Access-Control-Allow-Headers', 'location');
            res.header('Content-Type', 'application/json');
            res.header('location', config_1.default.get('links').user_uri + String(result));
            if (req.body.action === 'signup') {
                mailing_1.postMail(req.body.emailAddress, 'User Verification Code', 'Your Verification Code: ' + verificationCode);
                const codeDelivery = new models_1.CodeDelivery('email', 'EMAIL', req.body.emailAddress);
                const codeDeliveries = new models_1.CodeDeliveries([codeDelivery]);
                res.status(201).json(codeDeliveries);
            }
            else {
                mailing_1.postMail(req.body.emailAddress, 'Code to Change Password', 'Your Password Change Code: ' + verificationCode);
                res.status(201).json({
                    result,
                });
            }
        }
        else {
            res.status(400).json();
        }
    }
    async updateUserStatus(req, res) {
        /*const errorResponse: ErrorResponse = await validateToken(req.get(TOKEN_KEY), req.get(PROVIDER));
        if (errorResponse !== null) {
            return res.status(errorResponse.code).json(errorResponse);
        }*/
        const validationResponse = await validateRequiredFields(['userName', 'code'], req);
        if (validationResponse !== null) {
            return res.status(validationResponse.code).json(validationResponse);
        }
        const response = await services_1.userService.updateUserStatus(req.body.userName, req.body.code);
        if (response === null) {
            const errorResponse = await error_response_1.invalidUserNameOrEmail();
            return res.status(errorResponse.code).json(errorResponse);
        }
        if (response.errors.length > 0) {
            return res.status(response.code).json(response);
        }
        return res.status(200).json();
    }
    async updateUserByID(req, res, next) {
        const errorResponse = await info_commons_1.validateToken(req.get(info_commons_1.TOKEN_KEY), req.get(info_commons_1.PROVIDER));
        if (errorResponse !== null) {
            return res.status(errorResponse.code).json(errorResponse);
        }
        const result = await services_1.userService.updateUserByID(req.body, req.params.id);
        result === 1 ? res.status(200).json() : res.status(400).json();
    }
    async updateAllUsers(req, res, next) {
        const errorResponse = await info_commons_1.validateToken(req.get(info_commons_1.TOKEN_KEY), req.get(info_commons_1.PROVIDER));
        if (errorResponse !== null) {
            return res.status(errorResponse.code).json(errorResponse);
        }
        services_1.userService.updateAllUsers(req.body)
            .then((result) => result > 0 ? res.status(200).json() :
            res.status(400).json())
            .catch((err) => next(err));
    }
    async fetchUserByID(req, res, next) {
        const errorResponse = await info_commons_1.validateToken(req.get(info_commons_1.TOKEN_KEY), req.get(info_commons_1.PROVIDER));
        if (errorResponse !== null) {
            return res.status(errorResponse.code).json(errorResponse);
        }
        services_1.userService.fetchUserByID(req.params.id)
            .then((user) => user && Object.keys(user).length !== 0 ? res.json(user) :
            res.status(404).json())
            .catch((err) => next(err));
    }
    async fetchUserByUserName(req, res, next) {
        const errorResponse = await info_commons_1.validateToken(req.get(info_commons_1.TOKEN_KEY), req.get(info_commons_1.PROVIDER));
        if (errorResponse !== null) {
            return res.status(errorResponse.code).json(errorResponse);
        }
        services_1.userService.fetchUserByUserName(req.params.id)
            .then((user) => user && Object.keys(user).length !== 0 ? res.json(user) :
            res.status(404).json())
            .catch((err) => next(err));
    }
    async fetchAllUsers(req, res, next) {
        const errorResponse = await info_commons_1.validateToken(req.get(info_commons_1.TOKEN_KEY), req.get(info_commons_1.PROVIDER));
        if (errorResponse !== null) {
            return res.status(errorResponse.code).json(errorResponse);
        }
        services_1.userService.fetchAllUsers()
            .then((users) => res.json(users))
            .catch((err) => next(err));
    }
    async fetchRolesByUser(req, res, next) {
        const errorResponse = await info_commons_1.validateToken(req.get(info_commons_1.TOKEN_KEY), req.get(info_commons_1.PROVIDER));
        if (errorResponse !== null) {
            return res.status(errorResponse.code).json(errorResponse);
        }
        services_1.userService.fetchRolesByUser(req.params.id)
            .then((role) => role && Object.keys(role).length !== 0 ? res.json(role) :
            res.status(404).json())
            .catch((err) => next(err));
    }
    async fetchAllRoles(req, res, next) {
        const errorResponse = await info_commons_1.validateToken(req.get(info_commons_1.TOKEN_KEY), req.get(info_commons_1.PROVIDER));
        if (errorResponse !== null) {
            return res.status(errorResponse.code).json(errorResponse);
        }
        services_1.userService.fetchAllRoles()
            .then((roles) => res.json(roles))
            .catch((err) => next(err));
    }
    async createRole(req, res, next) {
        const validationResponse = await validateRequiredFields(['roleName'], req);
        if (validationResponse !== null) {
            return res.status(validationResponse.code).json(validationResponse);
        }
        const errorResponse = await info_commons_1.validateToken(req.get(info_commons_1.TOKEN_KEY), req.get(info_commons_1.PROVIDER));
        if (errorResponse !== null) {
            return res.status(errorResponse.code).json(errorResponse);
        }
        services_1.userService.createRole(req.body)
            .then((result) => result > 0 ?
            res.status(201).header('location', config_1.default.get('links').role_uri + String(result)).json()
            : res.status(400).json())
            .catch((err) => next(err));
    }
    async updateRoleByID(req, res, next) {
        const errorResponse = await info_commons_1.validateToken(req.get(info_commons_1.TOKEN_KEY), req.get(info_commons_1.PROVIDER));
        if (errorResponse !== null) {
            return res.status(errorResponse.code).json(errorResponse);
        }
        services_1.userService.updateRoleByID(req.body, req.params.id)
            .then((result) => result === 1 ? res.status(200).json() : res.status(400).json())
            .catch((err) => next(err));
    }
    async fetchAllUserMappings(req, res, next) {
        const errorResponse = await info_commons_1.validateToken(req.get(info_commons_1.TOKEN_KEY), req.get(info_commons_1.PROVIDER));
        if (errorResponse !== null) {
            return res.status(errorResponse.code).json(errorResponse);
        }
        services_1.userService.fetchAllUserMappings()
            .then((userMappings) => res.json(userMappings))
            .catch((err) => next(err));
    }
    async createUserMapping(req, res, next) {
        const validationResponse = await validateRequiredFields(['userId', 'roleId'], req);
        if (validationResponse !== null) {
            return res.status(validationResponse.code).json(validationResponse);
        }
        const errorResponse = await info_commons_1.validateToken(req.get(info_commons_1.TOKEN_KEY), req.get(info_commons_1.PROVIDER));
        if (errorResponse !== null) {
            return res.status(errorResponse.code).json(errorResponse);
        }
        services_1.userService.createUserMapping(req.body)
            .then((result) => {
            if (result > 0) {
                res.header('Access-Control-Allow-Origin', '*');
                res.header('Access-Control-Expose-Headers', 'location');
                res.header('Access-Control-Allow-Headers', 'location');
                res.header('Content-Type', 'json');
                res.header('location', config_1.default.get('links').usermapping_uri + String(result));
                res.status(201);
                next();
                res.send();
            }
            else {
                res.status(400).json();
            }
        });
    }
    async deleteUserByID(req, res, next) {
        const errorResponse = await info_commons_1.validateToken(req.get(info_commons_1.TOKEN_KEY), req.get(info_commons_1.PROVIDER));
        if (errorResponse !== null) {
            return res.status(errorResponse.code).json(errorResponse);
        }
        services_1.userService.deleteUserByID(req.params.id)
            .then((user) => user === 1 ? services_1.userService.deleteUserMappingByUserId(req.params.id)
            .then((userMapping) => userMapping === 0 || userMapping > 0 ?
            res.status(200).json() : res.status(404).json())
            .catch((err) => next(err)) : res.status(404).json())
            .catch((err) => next(err));
    }
    async deleteRoleById(req, res, next) {
        const errorResponse = await info_commons_1.validateToken(req.get(info_commons_1.TOKEN_KEY), req.get(info_commons_1.PROVIDER));
        if (errorResponse !== null) {
            return res.status(errorResponse.code).json(errorResponse);
        }
        services_1.userService.deleteRoleById(req.params.id)
            .then((role) => role === 1 ?
            services_1.userService.deleteUserMappingByRoleId(req.params.id)
                .then((userMapping) => userMapping === 0 || userMapping > 0 ?
                res.status(200).json() : res.status(404).json())
                .catch((err) => next(err)) : res.status(404).json())
            .catch((err) => next(err));
    }
    async deleteUserMapping(req, res, next) {
        const errorResponse = await info_commons_1.validateToken(req.get(info_commons_1.TOKEN_KEY), req.get(info_commons_1.PROVIDER));
        if (errorResponse !== null) {
            return res.status(errorResponse.code).json(errorResponse);
        }
        services_1.userService.deleteUserMapping(req.params.id)
            .then((userMapping) => userMapping === 1 ? res.status(200).json()
            : res.status(404).json())
            .catch((err) => next(err));
    }
    async fetchSubProjectByProjectID(req, res, next) {
        const errorResponse = await info_commons_1.validateToken(req.get(info_commons_1.TOKEN_KEY), req.get(info_commons_1.PROVIDER));
        if (errorResponse !== null) {
            return res.status(errorResponse.code).json(errorResponse);
        }
        services_1.userService.fetchSubProjectsByProjectID(req.params.projectId)
            .then((subProject) => subProject &&
            Object.keys(subProject).length === 0 || subProject &&
            Object.keys(subProject).length > 0 ? res.json(subProject) :
            res.status(404).json())
            .catch((err) => next(err));
    }
    async fetchUsersByRoleID(req, res, next) {
        const errorResponse = await info_commons_1.validateToken(req.get(info_commons_1.TOKEN_KEY), req.get(info_commons_1.PROVIDER));
        if (errorResponse !== null) {
            return res.status(errorResponse.code).json(errorResponse);
        }
        services_1.userService.fetchUsersByRoleID(req.params.id)
            .then((userMapping) => userMapping &&
            Object.keys(userMapping).length === 0 || userMapping &&
            Object.keys(userMapping).length > 0 ? res.json(userMapping) :
            res.status(404).json())
            .catch((err) => next(err));
    }
}
const validateRequiredFields = async (fields, request) => {
    const errors = [];
    let hasError = false;
    fields.forEach((field) => {
        if (!request.body.hasOwnProperty(field) || request.body[field].trim() === '') {
            const err = {
                domain: 'global', reason: constants_1.ErrorReasonEnum.MISSING_PARAMETER, message: field + ' is required', locationType: 'parameter', location: field,
                extendedHelp: 'http://request/help',
            };
            errors.push(err);
            hasError = true;
        }
    });
    if (hasError) {
        const error = new info_commons_1.ErrorResponse();
        error.code = 400;
        error.message = 'User is missing required fields';
        error.errors = errors;
        return error;
    }
    return null;
};
exports.default = new UserController();
//# sourceMappingURL=user.controller.js.map