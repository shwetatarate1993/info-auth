import { NextFunction, Request, Response } from 'express';
import { AuthService } from '../services/auth.service';
declare class AuthController {
    authService: AuthService;
    constructor(configDB: any, databaseId: string, dialect: string);
    authentication(req: Request, res: Response, next: NextFunction): Promise<import("express-serve-static-core").Response>;
    authorization(request: Request, response: Response): Promise<import("express-serve-static-core").Response>;
    forgotPassword(request: Request, response: Response): Promise<import("express-serve-static-core").Response>;
    resetPassword(request: Request, response: Response): Promise<import("express-serve-static-core").Response>;
    updatePassword(request: Request, response: Response): Promise<import("express-serve-static-core").Response>;
    resendCode(request: Request, response: Response): Promise<import("express-serve-static-core").Response>;
    refreshToken(request: Request, response: Response): Promise<import("express-serve-static-core").Response>;
    createUser(req: Request, res: Response, next: NextFunction): Promise<import("express-serve-static-core").Response>;
    updateUserStatus(req: Request, res: Response): Promise<import("express-serve-static-core").Response>;
}
declare const getAuthController: (configDB: any, databaseId: string, dialect: string) => AuthController;
export default getAuthController;
//# sourceMappingURL=auth.controller.d.ts.map