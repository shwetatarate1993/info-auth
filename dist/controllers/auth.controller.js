"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const v4_1 = __importDefault(require("uuid/v4"));
const isEmail_1 = __importDefault(require("validator/lib/isEmail"));
const config_1 = __importDefault(require("../config"));
const constants_1 = require("../constants");
const constants_2 = require("../constants");
const models_1 = require("../models");
const auth_service_1 = __importDefault(require("../services/auth.service"));
const encryption_1 = require("../utilities/encryption");
const error_response_1 = require("../utilities/error.response");
const mailing_1 = require("../utilities/mailing");
class AuthController {
    constructor(configDB, databaseId, dialect) {
        this.authService = auth_service_1.default(configDB, databaseId, dialect);
    }
    async authentication(req, res, next) {
        const errorResponse = await validateRequiredFields(['userName', 'password'], req);
        if (errorResponse !== null) {
            return res.status(errorResponse.code).json(errorResponse);
        }
        const { userName, password, relayState } = req.body;
        this.authService.authentication(userName, password, relayState)
            .then((user) => JSON.stringify(user).includes('"status":"SUCCESS"') ?
            res.json(user) : res.status(401).json(user))
            .catch((err) => next(err));
    }
    async authorization(request, response) {
        // ToDo Need to move to validator class
        const validationResponse = await validate(request);
        if (validationResponse !== null) {
            return response.status(validationResponse.code).json(validationResponse);
        }
        const tokenResponse = await this.authService.authorization(request.body.scope, request.body.client_id, request.body.user_name, request.body.password);
        console.log('tokenResponse::' + JSON.stringify(tokenResponse));
        if (tokenResponse === null) {
            const invalidCredentialResponse = await invalidCredential();
            return response.status(invalidCredentialResponse.code).json(invalidCredentialResponse);
        }
        const verificationErrorResponse = await verifyUserStatus(tokenResponse);
        if (verificationErrorResponse !== null) {
            return response.status(verificationErrorResponse.code).json(verificationErrorResponse);
        }
        // Type alias declaration
        const responseJson = {};
        const responseTypeArr = request.body.response_type.replace(/\s\s+/g, constants_1.SPACE)
            .split(constants_1.SPACE);
        if (responseTypeArr.includes(constants_1.ID_TOKEN)) {
            responseJson.id_token = tokenResponse.idToken;
        }
        if (responseTypeArr.includes(constants_1.TOKEN)) {
            responseJson.access_token = tokenResponse.accessToken;
            responseJson.refresh_token = tokenResponse.refreshToken;
        }
        responseJson.expires_in = tokenResponse.expiresIn;
        responseJson.token_type = tokenResponse.tokenType;
        response.json(responseJson);
    }
    async forgotPassword(request, response) {
        const validationResponse = await validateRequiredFields(['userName'], request);
        if (validationResponse !== null) {
            return response.status(validationResponse.code).json(validationResponse);
        }
        const codeDeliveries = await this.authService
            .forgotPassword(request.body.userName);
        if (codeDeliveries === null) {
            const errorResponse = await error_response_1.invalidUserNameOrEmail();
            return response.status(errorResponse.code).json(errorResponse);
        }
        return response.status(200).json(codeDeliveries);
    }
    async resetPassword(request, response) {
        const validationResponse = await validateRequiredFields(['userName', 'password',
            'code'], request);
        if (validationResponse !== null) {
            return response.status(validationResponse.code).json(validationResponse);
        }
        const resetPwdResponse = await this.authService.resetPassword(request.body.userName, request.body.password, request.body.code);
        if (resetPwdResponse === null) {
            const errorResponse = await error_response_1.invalidUserNameOrEmail();
            return response.status(errorResponse.code).json(errorResponse);
        }
        if (resetPwdResponse.errors.length > 0) {
            return response.status(resetPwdResponse.code).json(resetPwdResponse);
        }
        return response.status(200).json();
    }
    async updatePassword(request, response) {
        const validationResponse = await validateRequiredFields(['userName', 'oldPassword',
            'newPassword'], request);
        if (validationResponse !== null) {
            return response.status(validationResponse.code).json(validationResponse);
        }
        const resetPwdResponse = await this.authService.updatePassword(request.params.userName, request.body.oldPassword, request.body.newPassword);
        if (resetPwdResponse === null) {
            const errorResponse = await error_response_1.invalidUserNameOrEmail();
            return response.status(errorResponse.code).json(errorResponse);
        }
        if (resetPwdResponse.errors.length > 0) {
            return response.status(resetPwdResponse.code).json(resetPwdResponse);
        }
        return response.status(200).json();
    }
    async resendCode(request, response) {
        const validationResponse = await validateRequiredFields(['userName'], request);
        if (validationResponse !== null) {
            return response.status(validationResponse.code).json(validationResponse);
        }
        const codeDeliveries = await this.authService
            .resendCode(request.body.userName);
        if (codeDeliveries === null) {
            const errorResponse = await error_response_1.invalidUserNameOrEmail();
            return response.status(errorResponse.code).json(errorResponse);
        }
        return response.status(200).json(codeDeliveries);
    }
    async refreshToken(request, response) {
        const errorResponse = await info_commons_1.validateToken(request.get(constants_2.REFRESH_TOKEN_KEY), request.get(info_commons_1.PROVIDER));
        if (errorResponse !== null) {
            return response.status(errorResponse.code).json(errorResponse);
        }
        const validationResponse = await validate(request);
        if (validationResponse !== null) {
            return response.status(validationResponse.code).json(validationResponse);
        }
        const tokenResponse = await this.authService.refreshToken(request.body.scope, request.body.client_id, request.get(constants_2.REFRESH_TOKEN_KEY));
        if (tokenResponse === null) {
            const invalidCredentialResponse = await invalidCredential();
            return response.status(invalidCredentialResponse.code).json(invalidCredentialResponse);
        }
        if ('Token Mismatch' === tokenResponse.userStatus) {
            const misMatchedTokenResponse = await tokenMismatched();
            return response.status(misMatchedTokenResponse.code).json(misMatchedTokenResponse);
        }
        // Type alias declaration
        const responseJson = {};
        const responseTypeArr = request.body.response_type.replace(/\s\s+/g, constants_1.SPACE)
            .split(constants_1.SPACE);
        if (responseTypeArr.includes(constants_1.ID_TOKEN)) {
            responseJson.id_token = tokenResponse.idToken;
        }
        if (responseTypeArr.includes(constants_1.TOKEN)) {
            responseJson.access_token = tokenResponse.accessToken;
            responseJson.refresh_token = tokenResponse.refreshToken;
        }
        responseJson.expires_in = tokenResponse.expiresIn;
        responseJson.token_type = tokenResponse.tokenType;
        response.json(responseJson);
    }
    async createUser(req, res, next) {
        // Validate Mandatory Fields
        const mandatoryFieldsErrorResponse = await validateRequiredFields(['action',
            'password', 'emailAddress'], req);
        if (mandatoryFieldsErrorResponse !== null) {
            return res.status(mandatoryFieldsErrorResponse.code).json(mandatoryFieldsErrorResponse);
        }
        if (!isEmail_1.default(req.body.emailAddress)) {
            const emailFormatError = await error_response_1.invalidEmailFormat();
            return res.status(emailFormatError.code).json(emailFormatError);
        }
        if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{8,20}$/.test(req.body.password)) {
            const weakPasswordError = await error_response_1.weakPassword();
            return res.status(weakPasswordError.code).json(weakPasswordError);
        }
        const userList = await this.authService.fetchUserByEmailID(req.body.emailAddress);
        if (userList) {
            const userAlreadyExistError = await error_response_1.userAlreadyExist();
            return res.status(userAlreadyExistError.code).json(userAlreadyExistError);
        }
        const action = req.body.action;
        const verificationCode = v4_1.default().substring(0, 8);
        const encrypVerificationKey = encryption_1.keyEncryption(verificationCode);
        req.body.verificationKey = encrypVerificationKey;
        req.body.USER_AUTH_ID = v4_1.default();
        const result = await this.authService.createUser(req.body, req.body.action);
        if (result > 0) {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Expose-Headers', 'location');
            res.header('Access-Control-Allow-Headers', 'location');
            res.header('Content-Type', 'application/json');
            res.header('location', config_1.default.get('links').user_uri + String(result));
            console.log('action:::' + action);
            if (action === 'signup') {
                console.log('req.body.emailAddress:::' + req.body.emailAddress);
                mailing_1.postMail(req.body.emailAddress, 'User Verification Code', 'Your Verification Code: ' + verificationCode);
                const codeDelivery = new models_1.CodeDelivery('email', 'EMAIL', req.body.emailAddress);
                const codeDeliveries = new models_1.CodeDeliveries([codeDelivery]);
                res.status(201).json(codeDeliveries);
            }
            else {
                mailing_1.postMail(req.body.emailAddress, 'Code to Change Password', 'Your Password Change Code: ' + verificationCode);
                res.status(201).json({
                    result,
                });
            }
        }
        else {
            res.status(400).json();
        }
    }
    async updateUserStatus(req, res) {
        /*const errorResponse: ErrorResponse = await validateToken(req.get(TOKEN_KEY), req.get(PROVIDER));
        if (errorResponse !== null) {
            return res.status(errorResponse.code).json(errorResponse);
        }*/
        const validationResponse = await validateRequiredFields(['userName', 'code'], req);
        if (validationResponse !== null) {
            return res.status(validationResponse.code).json(validationResponse);
        }
        const response = await this.authService.updateUserStatus(req.body.userName, req.body.code);
        if (response === null) {
            const errorResponse = await error_response_1.invalidUserNameOrEmail();
            return res.status(errorResponse.code).json(errorResponse);
        }
        if (response.errors.length > 0) {
            return res.status(response.code).json(response);
        }
        return res.status(200).json();
    }
}
const validate = async (request) => {
    // Validate Mandatory Fields
    const mandatoryFieldsErrorResponse = await validateMandatoryFields(request);
    if (mandatoryFieldsErrorResponse !== null) {
        return mandatoryFieldsErrorResponse;
    }
    // Validate response_type values
    const invalidResponseTypeErrorResponse = await validateResponseType(request);
    if (invalidResponseTypeErrorResponse !== null) {
        return invalidResponseTypeErrorResponse;
    }
    // Validate scope values
    const invalidScopeErrorResponse = await validateScope(request);
    if (invalidScopeErrorResponse !== null) {
        return invalidScopeErrorResponse;
    }
    return null;
};
const validateMandatoryFields = async (request) => {
    const mandatoryFields = ['client_id', 'scope', 'response_type', 'user_name', 'password'];
    const errors = [];
    let hasError = false;
    // mandatoryFields.forEach((field: string) => {
    for (const field of mandatoryFields) {
        if (request.url === '/refresh' && (field === 'user_name' || field === 'password')) {
            continue;
        }
        if (!(field in request.body) || request.body[field].trim() === '') {
            const err = {
                domain: 'global', reason: constants_2.ErrorReasonEnum.MISSING_PARAMETER, message: field + ' is required', locationType: 'parameter', location: field,
                extendedHelp: 'http://request/help',
            };
            errors.push(err);
            hasError = true;
        }
    }
    if ('scope' in request.body) {
        const scopeArray = request.body.scope.replace(/\s\s+/g, ' ').split(' ');
        if (!scopeArray.includes('openid')) {
            const err = {
                domain: 'global', reason: constants_2.ErrorReasonEnum.MISSING_PARAMETER_VALUE,
                message: 'openid is required as a scope value', locationType: 'parameter value',
                location: 'openid', extendedHelp: 'http://request/help',
            };
            errors.push(err);
            hasError = true;
        }
    }
    if (hasError) {
        const error = new info_commons_1.ErrorResponse();
        error.code = 400;
        error.message = 'User is missing required fields';
        error.errors = errors;
        return error;
    }
    return null;
};
const validateResponseType = async (request) => {
    const errors = [];
    let hasError = false;
    const responseTypeArr = request.body.response_type.replace(/\s\s+/g, ' ').split(' ');
    for (const resType of responseTypeArr) {
        const parsedResponseType = constants_1.parseResponseTypeEnum(resType);
        if (parsedResponseType === null) {
            const err = {
                domain: 'global', reason: constants_2.ErrorReasonEnum.INVALID_PARAMETER,
                message: 'Invalid string value: \'' + resType + '\' ' +
                    'Allowed values: [id_token token]', locationType: 'Request Parameter',
                location: 'responseType', extendedHelp: 'http://request/help;',
            };
            errors.push(err);
            hasError = true;
        }
    }
    if (hasError) {
        const error = new info_commons_1.ErrorResponse();
        error.code = 422;
        error.message = 'Invalid Parameter';
        error.errors = errors;
        return error;
    }
    return null;
};
const validateScope = async (request) => {
    const errors = [];
    let hasError = false;
    const scopeArr = request.body.scope.replace(/\s\s+/g, ' ').split(' ');
    for (const scope of scopeArr) {
        const parsedScope = constants_1.parseScopeEnum(scope);
        if (parsedScope === null) {
            const err = {
                domain: 'global', reason: constants_2.ErrorReasonEnum.INVALID_PARAMETER,
                message: 'Invalid string value: \'' + scope + '\' Allowed values: ' +
                    '[openid email profile phone address]', locationType: 'Request Parameter',
                location: 'scope', extendedHelp: 'http://request/help',
            };
            errors.push(err);
            hasError = true;
        }
    }
    if (hasError) {
        const error = new info_commons_1.ErrorResponse();
        error.code = 422;
        error.message = 'Invalid Parameter';
        error.errors = errors;
        return error;
    }
    return null;
};
const invalidCredential = async () => {
    const error = new info_commons_1.ErrorResponse();
    error.code = 403;
    error.message = 'Invalid Credential';
    error.errors = [{
            domain: 'global', reason: constants_2.ErrorReasonEnum.INVALID_CREDENTIAL,
            message: 'Invalid userName or password', locationType: 'Request Parameter',
            location: 'userName or password', extendedHelp: 'http://request/help',
        }];
    return error;
};
const tokenMismatched = async () => {
    const error = new info_commons_1.ErrorResponse();
    error.code = 403;
    error.message = 'Token Mismatch';
    error.errors = [{
            domain: 'global', reason: constants_2.ErrorReasonEnum.TOKEN_MISMATCH,
            message: 'Token not matching with user\'s token', locationType: 'Request Header',
            location: 'refresh-token', extendedHelp: 'http://request/help',
        }];
    return error;
};
const verifyUserStatus = async (tokenResponse) => {
    const errors = [];
    let hasError = false;
    if (tokenResponse.userStatus === 'PENDING_VERIFICATION') {
        // Type alias declaration
        const err = {};
        err.reason = constants_2.ErrorReasonEnum.PENDING_VERIFICATION;
        err.message = 'User verification is pending';
        errors.push(err);
        hasError = true;
    }
    if (tokenResponse.userStatus === 'DEACTIVATED') {
        // Type alias declaration
        const err = {};
        err.reason = constants_2.ErrorReasonEnum.DEACTIVATED;
        err.message = 'User has been deactivated';
        errors.push(err);
        hasError = true;
    }
    if (tokenResponse.userStatus === 'LOCKED') {
        // Type alias declaration
        const err = {};
        err.reason = constants_2.ErrorReasonEnum.LOCKED;
        err.message = 'Multiple Invalid Login Attempts';
        errors.push(err);
        hasError = true;
    }
    if (tokenResponse.userStatus === 'PENDING_CHANGE_PASSWORD') {
        // Type alias declaration
        const err = {};
        err.reason = constants_2.ErrorReasonEnum.PENDING_CHANGE_PASSWORD;
        err.message = 'User is not Active';
        errors.push(err);
        hasError = true;
    }
    if (hasError) {
        const error = new info_commons_1.ErrorResponse();
        error.code = 422;
        error.message = 'Incomplete User Profile';
        error.errors = errors;
        return error;
    }
    return null;
};
const validateRequiredFields = async (fields, request) => {
    const errors = [];
    let hasError = false;
    fields.forEach((field) => {
        if (request.body[field].trim() === '') {
            const err = {
                domain: 'global', reason: constants_2.ErrorReasonEnum.MISSING_PARAMETER, message: field + ' is required', locationType: 'parameter', location: field,
                extendedHelp: 'http://request/help',
            };
            errors.push(err);
            hasError = true;
        }
    });
    if (hasError) {
        const error = new info_commons_1.ErrorResponse();
        error.code = 400;
        error.message = 'User is missing required fields';
        error.errors = errors;
        return error;
    }
    return null;
};
const getAuthController = (configDB, databaseId, dialect) => {
    return new AuthController(configDB, databaseId, dialect);
};
exports.default = getAuthController;
//# sourceMappingURL=auth.controller.js.map