"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const node_mocks_http_1 = __importDefault(require("node-mocks-http"));
const sinon_1 = __importDefault(require("sinon"));
const constants_1 = require("../../constants");
const controllers_1 = require("../../controllers");
const services_1 = require("../../services");
const mailing = __importStar(require("../../utilities/mailing"));
describe('CREATE USER', async () => {
    const mailingServiceMock = sinon_1.default.mock(mailing);
    test.skip('Mandatory Validation Failure', async () => {
        const request = node_mocks_http_1.default.createRequest({
            body: {
                action: 'signup',
                userName: '',
                password: '',
                firstName: '',
                emailAddress: '',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        await controllers_1.userController.createUser(request, response, null);
        sinon_1.default.assert.match(response.statusCode, 400);
        sinon_1.default.assert.match(JSON.parse(response._getData()).errors.length, 4);
        sinon_1.default.assert.match(JSON.parse(response._getData()).errors[0].reason, constants_1.ErrorReasonEnum.MISSING_PARAMETER);
        sinon_1.default.assert.match(JSON.parse(response._getData()).errors[1].reason, constants_1.ErrorReasonEnum.MISSING_PARAMETER);
    });
    test.skip('Invalid Email Format', async () => {
        const request = node_mocks_http_1.default.createRequest({
            body: {
                action: 'signup',
                userName: 'ankur',
                password: 'pwd',
                firstName: 'Ankur G',
                emailAddress: 'abc',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        await controllers_1.userController.createUser(request, response, null);
        sinon_1.default.assert.match(response.statusCode, 422);
        sinon_1.default.assert.match(JSON.parse(response._getData()).errors[0].reason, constants_1.ErrorReasonEnum.INVALID_EMAIL_FORMAT);
    });
    test.skip('Weak Password', async () => {
        const request = node_mocks_http_1.default.createRequest({
            body: {
                action: 'signup',
                userName: 'ankur',
                password: 'pwd',
                firstName: 'Ankur G',
                emailAddress: 'ag@gmail.com',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        await controllers_1.userController.createUser(request, response, null);
        sinon_1.default.assert.match(response.statusCode, 422);
        sinon_1.default.assert.match(JSON.parse(response._getData()).errors[0].reason, constants_1.ErrorReasonEnum.WEAK_PASSWORD);
    });
    test.skip('Create User(Signup) with success response', async () => {
        const request = node_mocks_http_1.default.createRequest({
            body: {
                action: 'signup',
                userName: 'ankur',
                password: 'Ankurg@123',
                firstName: 'Ankur G',
                emailAddress: 'ag@gmail.com',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        const createUserMock = sinon_1.default.mock(services_1.userService);
        createUserMock.expects('createUser').returns(1);
        mailingServiceMock.expects('postMail').returns('success');
        await controllers_1.userController.createUser(request, response, null);
        sinon_1.default.assert.match(response.statusCode, 201);
        sinon_1.default.assert.match(JSON.parse(response._getData()).codeDeliveries[0].attributeName, 'email');
        createUserMock.restore();
    });
    test.skip('Create User(Register) with success response', async () => {
        const request = node_mocks_http_1.default.createRequest({
            body: {
                action: 'register',
                userName: 'ankur',
                password: 'Ankurg@123',
                firstName: 'Ankur G',
                emailAddress: 'ag@gmail.com',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        const createUserMock = sinon_1.default.mock(services_1.userService);
        createUserMock.expects('createUser').returns(1);
        mailingServiceMock.expects('postMail').returns('success');
        await controllers_1.userController.createUser(request, response, null);
        sinon_1.default.assert.match(response.statusCode, 201);
        sinon_1.default.assert.match(JSON.parse(response._getData()).Error.reason, constants_1.ErrorReasonEnum.PENDING_CHANGE_PASSWORD);
        createUserMock.restore();
        mailingServiceMock.restore();
    });
});
describe('UPDATE USER STATUS', async () => {
    test.skip('Mandatory Validation Failure', async () => {
        const request = node_mocks_http_1.default.createRequest({
            body: {
                userName: '',
                code: '',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        await controllers_1.userController.updateUserStatus(request, response);
        sinon_1.default.assert.match(response.statusCode, 400);
        sinon_1.default.assert.match(JSON.parse(response._getData()).errors.length, 2);
        sinon_1.default.assert.match(JSON.parse(response._getData()).errors[0].reason, constants_1.ErrorReasonEnum.MISSING_PARAMETER);
        sinon_1.default.assert.match(JSON.parse(response._getData()).errors[1].reason, constants_1.ErrorReasonEnum.MISSING_PARAMETER);
    });
    test.skip('Invalid UserName or Email', async () => {
        const request = node_mocks_http_1.default.createRequest({
            body: {
                userName: 'ankur',
                code: 'abc',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        const updateUserStatusMock = sinon_1.default.mock(services_1.userService);
        updateUserStatusMock.expects('updateUserStatus').returns(null);
        await controllers_1.userController.updateUserStatus(request, response);
        sinon_1.default.assert.match(response.statusCode, 403);
        sinon_1.default.assert.match(JSON.parse(response._getData()).errors[0].reason, constants_1.ErrorReasonEnum.INVALID_USERNAME);
        updateUserStatusMock.restore();
    });
    test.skip('Update Password Status(Confirm Signup) with Success Response', async () => {
        const request = node_mocks_http_1.default.createRequest({
            body: {
                userName: 'ankur',
                code: 'abc',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        const error = new info_commons_1.ErrorResponse();
        error.errors = [];
        const updateUserStatusMock = sinon_1.default.mock(services_1.userService);
        updateUserStatusMock.expects('updateUserStatus').returns(error);
        await controllers_1.userController.updateUserStatus(request, response);
        sinon_1.default.assert.match(response.statusCode, 200);
        updateUserStatusMock.restore();
    });
});
//# sourceMappingURL=user.controller.test.js.map