"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const validation = __importStar(require("info-commons"));
const info_commons_1 = require("info-commons");
const node_mocks_http_1 = __importDefault(require("node-mocks-http"));
const sinon_1 = __importDefault(require("sinon"));
const constants_1 = require("../../constants");
const controllers_1 = require("../../controllers");
const models_1 = require("../../models");
const authController = controllers_1.getAuthController(info_commons_1.config.get(constants_1.DB), constants_1.MOCK, 'sqlite3');
describe('AUTHORIZATION', async () => {
    test('Mandatory Validation Failure', async () => {
        const request = node_mocks_http_1.default.createRequest({
            method: 'POST',
            url: 'v1/authorize',
            body: {
                client_id: '123',
                redirect_uri: 'https://localhost.com',
                scope: 'openid profile email phone address',
                response_type: 'id_token',
                state: 'abc',
                password: 'pwd',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        await authController.authorization(request, response);
        sinon_1.default.assert.match(response.statusCode, 400);
        sinon_1.default.assert.match(JSON.parse(response._getData()).errors[0].reason, constants_1.ErrorReasonEnum.MISSING_PARAMETER);
    });
    test('Invalid Response Type', async () => {
        const request = node_mocks_http_1.default.createRequest({
            method: 'POST',
            url: 'v1/authorize',
            body: {
                client_id: '123',
                redirect_uri: 'https://localhost.com',
                scope: 'openid profile email phone address',
                response_type: 'abc',
                state: 'abc',
                user_name: 'ankur',
                password: 'pwd',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        await authController.authorization(request, response);
        sinon_1.default.assert.match(response.statusCode, 422);
        sinon_1.default.assert.match(JSON.parse(response._getData()).errors[0].reason, constants_1.ErrorReasonEnum.INVALID_PARAMETER);
    });
    test('Invalid Scope', async () => {
        const request = node_mocks_http_1.default.createRequest({
            method: 'POST',
            url: 'v1/authorize',
            body: {
                client_id: '123',
                redirect_uri: 'https://localhost.com',
                scope: 'openid abc',
                response_type: 'id_token',
                state: 'abc',
                user_name: 'ankur',
                password: 'pwd',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        await authController.authorization(request, response);
        sinon_1.default.assert.match(response.statusCode, 422);
        sinon_1.default.assert.match(JSON.parse(response._getData()).errors[0].reason, constants_1.ErrorReasonEnum.INVALID_PARAMETER);
    });
    test('Invalid Credential', async () => {
        const request = node_mocks_http_1.default.createRequest({
            method: 'POST',
            url: 'v1/authorize',
            body: {
                client_id: '123',
                redirect_uri: 'https://localhost.com',
                scope: 'openid profile email phone address',
                response_type: 'id_token',
                state: 'abc',
                user_name: 'ankur',
                password: 'pwd',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        const authorizationMock = sinon_1.default.mock(authController.authService);
        authorizationMock.expects('authorization').returns(null);
        await authController.authorization(request, response);
        sinon_1.default.assert.match(response.statusCode, 403);
        sinon_1.default.assert.match(JSON.parse(response._getData()).errors[0].reason, constants_1.ErrorReasonEnum.INVALID_CREDENTIAL);
        authorizationMock.restore();
    });
    test('Incomplete User Profile', async () => {
        const request = node_mocks_http_1.default.createRequest({
            method: 'POST',
            url: 'v1/authorize',
            body: {
                client_id: '123',
                redirect_uri: 'https://localhost.com',
                scope: 'openid profile email phone address',
                response_type: 'id_token',
                state: 'abc',
                user_name: 'ankur',
                password: 'pwd',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        const tokenResponse = new models_1.TokenResponse(null, null, null, 3600, 'Bearer', 'PENDING_VERIFICATION');
        const authorizationMock = sinon_1.default.mock(authController.authService);
        authorizationMock.expects('authorization').returns(tokenResponse);
        await authController.authorization(request, response);
        sinon_1.default.assert.match(response.statusCode, 422);
        sinon_1.default.assert.match(JSON.parse(response._getData()).message, 'Incomplete User Profile');
        authorizationMock.restore();
    });
    test('Successfull Authorization', async () => {
        const request = node_mocks_http_1.default.createRequest({
            method: 'POST',
            url: 'v1/authorize',
            body: {
                client_id: '123',
                redirect_uri: 'https://localhost.com',
                scope: 'openid email',
                response_type: 'token',
                state: 'abc',
                user_name: 'ankur',
                password: 'pwd',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        const tokenResponse = new models_1.TokenResponse('abc', 'abc', 'abc', 3600, 'Bearer', 'ACTIVE');
        const authorizationMock = sinon_1.default.mock(authController.authService);
        authorizationMock.expects('authorization').returns(tokenResponse);
        await authController.authorization(request, response);
        sinon_1.default.assert.match(JSON.parse(response._getData()).access_token, 'abc');
        sinon_1.default.assert.match(JSON.parse(response._getData()).token_type, 'Bearer');
        authorizationMock.restore();
    });
});
describe('FORGOT PASSWORD', async () => {
    test('Mandatory Validation Failure', async () => {
        const request = node_mocks_http_1.default.createRequest({
            body: {
                userName: '',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        await authController.forgotPassword(request, response);
        sinon_1.default.assert.match(response.statusCode, 400);
        sinon_1.default.assert.match(JSON.parse(response._getData()).errors[0].reason, constants_1.ErrorReasonEnum.MISSING_PARAMETER);
    });
    test('Invalid UserName or Email', async () => {
        const request = node_mocks_http_1.default.createRequest({
            body: {
                userName: 'ag',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        const forgotPasswordMock = sinon_1.default.mock(authController.authService);
        forgotPasswordMock.expects('forgotPassword').returns(null);
        await authController.forgotPassword(request, response);
        sinon_1.default.assert.match(response.statusCode, 403);
        sinon_1.default.assert.match(JSON.parse(response._getData()).errors[0].reason, constants_1.ErrorReasonEnum.INVALID_USERNAME);
        forgotPasswordMock.restore();
    });
    test('Forgot Password with Success Response', async () => {
        const request = node_mocks_http_1.default.createRequest({
            body: {
                userName: 'ankur',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        const codeDelivery = new models_1.CodeDelivery('email', 'EMAIL', 'ag@gmail.com');
        const forgotPasswordMock = sinon_1.default.mock(authController.authService);
        forgotPasswordMock.expects('forgotPassword').returns(new models_1.CodeDeliveries([codeDelivery]));
        await authController.forgotPassword(request, response);
        sinon_1.default.assert.match(response.statusCode, 200);
        sinon_1.default.assert.match(JSON.parse(response._getData()).codeDeliveries[0].attributeName, 'email');
        forgotPasswordMock.restore();
    });
});
describe('RESET PASSWORD', async () => {
    test('Mandatory Validation Failure', async () => {
        const request = node_mocks_http_1.default.createRequest({
            body: {
                userName: '',
                password: '',
                code: '',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        await authController.resetPassword(request, response);
        sinon_1.default.assert.match(response.statusCode, 400);
        sinon_1.default.assert.match(JSON.parse(response._getData()).errors[0].reason, constants_1.ErrorReasonEnum.MISSING_PARAMETER);
    });
    test('Invalid UserName or Email', async () => {
        const request = node_mocks_http_1.default.createRequest({
            body: {
                userName: 'ag',
                password: 'pwd',
                code: 'abc',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        const resetPasswordMock = sinon_1.default.mock(authController.authService);
        resetPasswordMock.expects('resetPassword').returns(null);
        await authController.resetPassword(request, response);
        sinon_1.default.assert.match(response.statusCode, 403);
        sinon_1.default.assert.match(JSON.parse(response._getData()).errors[0].reason, constants_1.ErrorReasonEnum.INVALID_USERNAME);
        resetPasswordMock.restore();
    });
    test('Invalid Code', async () => {
        const request = node_mocks_http_1.default.createRequest({
            body: {
                userName: 'ankur',
                password: 'pwd',
                code: 'abc',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        const error = new info_commons_1.ErrorResponse();
        error.code = 400;
        error.message = 'Invalid Passcode';
        error.errors = [{
                domain: 'global', reason: constants_1.ErrorReasonEnum.INVALID_PWD_RESET_CODE,
                message: 'Invalid Code', locationType: 'Request Body',
                location: 'code', extendedHelp: 'http://request/help',
            }];
        const resetPasswordMock = sinon_1.default.mock(authController.authService);
        resetPasswordMock.expects('resetPassword').returns(error);
        await authController.resetPassword(request, response);
        sinon_1.default.assert.match(response.statusCode, 400);
        sinon_1.default.assert.match(JSON.parse(response._getData()).errors[0].reason, constants_1.ErrorReasonEnum.INVALID_PWD_RESET_CODE);
        resetPasswordMock.restore();
    });
    test('Reset Password with Success Response', async () => {
        const request = node_mocks_http_1.default.createRequest({
            body: {
                userName: 'ankur',
                password: 'pwd',
                code: 'abc',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        const error = new info_commons_1.ErrorResponse();
        error.errors = [];
        const resetPasswordMock = sinon_1.default.mock(authController.authService);
        resetPasswordMock.expects('resetPassword').returns(error);
        await authController.resetPassword(request, response);
        sinon_1.default.assert.match(response.statusCode, 200);
        resetPasswordMock.restore();
    });
});
describe('UPDATE PASSWORD', async () => {
    test('Mandatory Validation Failure', async () => {
        const request = node_mocks_http_1.default.createRequest({
            body: {
                userName: '',
                oldPassword: '',
                newPassword: '',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        await authController.updatePassword(request, response);
        sinon_1.default.assert.match(response.statusCode, 400);
        sinon_1.default.assert.match(JSON.parse(response._getData()).errors[0].reason, constants_1.ErrorReasonEnum.MISSING_PARAMETER);
    });
    test('Invalid UserName or Email', async () => {
        const request = node_mocks_http_1.default.createRequest({
            body: {
                userName: 'ag',
                oldPassword: 'pwd',
                newPassword: 'abc',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        const updatePasswordMock = sinon_1.default.mock(authController.authService);
        updatePasswordMock.expects('updatePassword').returns(null);
        await authController.updatePassword(request, response);
        sinon_1.default.assert.match(response.statusCode, 403);
        sinon_1.default.assert.match(JSON.parse(response._getData()).errors[0].reason, constants_1.ErrorReasonEnum.INVALID_USERNAME);
        updatePasswordMock.restore();
    });
    test('Invalid Old Password', async () => {
        const request = node_mocks_http_1.default.createRequest({
            body: {
                userName: 'ankur',
                oldPassword: 'def',
                newPassword: 'abc',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        const error = new info_commons_1.ErrorResponse();
        error.code = 400;
        error.message = 'Invalid Old Password';
        error.errors = [{
                domain: 'global', reason: constants_1.ErrorReasonEnum.INVALID_OLD_PWD,
                message: 'Invalid Old Password', locationType: 'Request Body',
                location: 'Old Password', extendedHelp: 'http://request/help',
            }];
        const updatePasswordMock = sinon_1.default.mock(authController.authService);
        updatePasswordMock.expects('updatePassword').returns(error);
        await authController.updatePassword(request, response);
        sinon_1.default.assert.match(response.statusCode, 400);
        sinon_1.default.assert.match(JSON.parse(response._getData()).errors[0].reason, constants_1.ErrorReasonEnum.INVALID_OLD_PWD);
        updatePasswordMock.restore();
    });
    test('Update Password with Success Response', async () => {
        const request = node_mocks_http_1.default.createRequest({
            body: {
                userName: 'ankur',
                oldPassword: 'pwd',
                newPassword: 'abc',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        const error = new info_commons_1.ErrorResponse();
        error.errors = [];
        const updatePasswordMock = sinon_1.default.mock(authController.authService);
        updatePasswordMock.expects('updatePassword').returns(error);
        await authController.updatePassword(request, response);
        sinon_1.default.assert.match(response.statusCode, 200);
        updatePasswordMock.restore();
    });
});
describe('RESEND CODE', async () => {
    test('Mandatory Validation Failure', async () => {
        const request = node_mocks_http_1.default.createRequest({
            body: {
                userName: '',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        await authController.resendCode(request, response);
        sinon_1.default.assert.match(response.statusCode, 400);
        sinon_1.default.assert.match(JSON.parse(response._getData()).errors[0].reason, constants_1.ErrorReasonEnum.MISSING_PARAMETER);
    });
    test('Invalid UserName or Email', async () => {
        const request = node_mocks_http_1.default.createRequest({
            body: {
                userName: 'ag',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        const resendCodeMock = sinon_1.default.mock(authController.authService);
        resendCodeMock.expects('resendCode').returns(null);
        await authController.resendCode(request, response);
        sinon_1.default.assert.match(response.statusCode, 403);
        sinon_1.default.assert.match(JSON.parse(response._getData()).errors[0].reason, constants_1.ErrorReasonEnum.INVALID_USERNAME);
        resendCodeMock.restore();
    });
    test('Resend Code with Success Response', async () => {
        const request = node_mocks_http_1.default.createRequest({
            body: {
                userName: 'ankur',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        const codeDelivery = new models_1.CodeDelivery('email', 'EMAIL', 'ag@gmail.com');
        const resendCodeMock = sinon_1.default.mock(authController.authService);
        resendCodeMock.expects('resendCode').returns(new models_1.CodeDeliveries([codeDelivery]));
        await authController.resendCode(request, response);
        sinon_1.default.assert.match(response.statusCode, 200);
        sinon_1.default.assert.match(JSON.parse(response._getData()).codeDeliveries[0].attributeName, 'email');
        resendCodeMock.restore();
    });
});
describe('REFRESH', async () => {
    const validateTokenMock = sinon_1.default.mock(validation);
    const refreshTokenMock = sinon_1.default.mock(authController.authService);
    test('Trying Refresh Token with Invalid Credential', async () => {
        const request = node_mocks_http_1.default.createRequest({
            method: 'POST',
            url: 'v1/refresh',
            body: {
                client_id: '123',
                redirect_uri: 'https://localhost.com',
                scope: 'openid email',
                response_type: 'token',
                state: 'abc',
                user_name: 'ankur',
                password: 'pwd',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        validateTokenMock.expects('validateToken').returns(null);
        refreshTokenMock.expects('refreshToken').returns(null);
        await authController.refreshToken(request, response);
        sinon_1.default.assert.match(response.statusCode, 403);
        sinon_1.default.assert.match(JSON.parse(response._getData()).errors[0].reason, constants_1.ErrorReasonEnum.INVALID_CREDENTIAL);
    });
    test('Mismatch Token', async () => {
        const request = node_mocks_http_1.default.createRequest({
            method: 'POST',
            url: 'v1/refresh',
            body: {
                client_id: '123',
                redirect_uri: 'https://localhost.com',
                scope: 'openid email',
                response_type: 'token',
                state: 'abc',
                user_name: 'ankur',
                password: 'pwd',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        validateTokenMock.expects('validateToken').returns(null);
        const tokenResponse = new models_1.TokenResponse('abc', 'abc', 'def', 3600, 'Bearer', 'Token Mismatch');
        refreshTokenMock.expects('refreshToken').returns(tokenResponse);
        await authController.refreshToken(request, response);
        sinon_1.default.assert.match(response.statusCode, 403);
        sinon_1.default.assert.match(JSON.parse(response._getData()).errors[0].reason, constants_1.ErrorReasonEnum.TOKEN_MISMATCH);
    });
    test('Successfull Refresh', async () => {
        const request = node_mocks_http_1.default.createRequest({
            method: 'POST',
            url: 'v1/refresh',
            body: {
                client_id: '123',
                redirect_uri: 'https://localhost.com',
                scope: 'openid email',
                response_type: 'token',
                state: 'abc',
                user_name: 'ankur',
                password: 'pwd',
            },
        });
        const response = node_mocks_http_1.default.createResponse();
        validateTokenMock.expects('validateToken').returns(null);
        const tokenResponse = new models_1.TokenResponse('abc', 'abc', 'def', 3600, 'Bearer', 'Valid Token');
        refreshTokenMock.expects('refreshToken').returns(tokenResponse);
        await authController.refreshToken(request, response);
        sinon_1.default.assert.match(JSON.parse(response._getData()).access_token, 'abc');
        sinon_1.default.assert.match(JSON.parse(response._getData()).refresh_token, 'def');
        sinon_1.default.assert.match(JSON.parse(response._getData()).token_type, 'Bearer');
        validateTokenMock.restore();
        refreshTokenMock.restore();
    });
});
//# sourceMappingURL=auth.controller.test.js.map