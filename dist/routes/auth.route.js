"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const info_commons_1 = require("info-commons");
const constants_1 = require("../constants");
const controllers_1 = require("../controllers");
const router = express_1.default.Router();
const authController = controllers_1.getAuthController(info_commons_1.config.get(constants_1.DB), constants_1.METADB, info_commons_1.DIALECT);
// router.get('/user/:userId/code/:code', authController.);
router.post('/authorize', authController.authorization.bind(authController));
router.post('/authentication', authController.authentication.bind(authController));
router.post('/user/password/reset', authController.forgotPassword.bind(authController));
router.post('/user/password', authController.resetPassword.bind(authController));
router.post('/refresh', authController.refreshToken.bind(authController));
router.post('/signUp', authController.createUser.bind(authController));
router.post('/confirm', authController.updateUserStatus.bind(authController));
router.put('/user/:userName/password', authController.updatePassword.bind(authController));
router.put('/user/code/resend', authController.resendCode.bind(authController));
// router.put('/user/code/:code', authController.);
exports.default = router;
//# sourceMappingURL=auth.route.js.map