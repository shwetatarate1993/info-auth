"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const errorhandler_1 = __importDefault(require("errorhandler"));
const app_1 = __importDefault(require("./app"));
const index_1 = __importDefault(require("./config/index"));
if (process.env.NODE_ENV === 'development') {
    // only use in development
    app_1.default.use(errorhandler_1.default());
}
const port = index_1.default.get('port');
/**
 * Start Express server.
 */
const server = app_1.default.listen(port);
exports.default = server;
//# sourceMappingURL=server.js.map