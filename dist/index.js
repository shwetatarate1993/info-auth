"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const constants_1 = require("./constants");
const services_1 = require("./services");
exports.authService = services_1.getAuthService(info_commons_1.config.get(constants_1.DB), constants_1.METADB, info_commons_1.DIALECT);
var services_2 = require("./services");
exports.AuthService = services_2.AuthService;
exports.userService = services_2.userService;
exports.UserService = services_2.UserService;
//# sourceMappingURL=index.js.map