"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    createUserRequest: {
        userId: '1114',
        userName: 'Sankalp',
        password: 'abc',
        firstName: 'Sankalp',
        lastName: 'D',
        emailAddress: 'sd798@gmail.com',
    },
    updateUserRequest: {
        lastName: 'Dhekwar',
        emailAddress: 'sd1989@gmail.com',
    },
    updateAllUsersRequest: {
        password: 'pwd',
    },
    createRoleRequest: {
        roleId: '4',
        roleName: 'DBA',
    },
    updateRoleRequest: {
        roleName: 'Database Administrator',
    },
};
//# sourceMappingURL=user.request.js.map