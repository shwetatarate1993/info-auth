"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const knex_config_1 = require("knex-config");
const constants_1 = require("../constants");
const knex = knex_config_1.knexClient(info_commons_1.config.get(constants_1.DB), constants_1.MOCK, 'sqlite3');
const pwdEncrypted = 'e534a11775e3e9416e0e45a2193960f4$e6db998ac053572a0' +
    'e0dca0e62d6af076513f717def07cdddf96ba3680a85df6';
// USERDETAIL TABLE
exports.createUserDetailTable = async () => {
    return knex.schema.createTableIfNotExists('USERDETAIL', (table) => {
        table.integer('USERID');
        table.string('USERNAME');
        table.string('PASSWORD');
        table.string('FIRSTNAME');
        table.string('LASTNAME');
        table.string('USERSTATUS');
        table.string('EMAILADDRESS');
        table.string('VERIFICATION_KEY');
        table.string('USER_LOGIN_ID');
        table.string('USER_KEY');
        table.string('CONTACTNUMBER');
        table.string('PROFILESTATUS');
        table.blob('PHOTO');
        table.string('FCM_TOKEN');
        table.integer('MOBILE_NUMBER');
        table.date('DATE_OF_BIRTH');
        table.string('GENDER');
        table.string('CASTE');
        table.integer('OFFICE_PHONE_NUMBER');
        table.string('QUALIFICATION');
        table.string('MIDDLE_NAME');
        table.string('POSITION_TITLE');
        table.string('COMPANY_EMAIL_ADDRESS');
        table.string('COMPANY_LINKEDIN_ID');
        table.timestamp('PASSWORD_CHANGED');
        table.string('PASSWORD_STATUS');
        table.integer('EMAIL_VERIFIED');
        table.integer('PHONE_NUMBER_VERIFIED');
        table.string('PHOTO_URL');
        table.string('NICK_NAME');
        table.string('PREFERRED_USERNAME');
        table.string('PROFILE');
        table.string('WEBSITE');
        table.string('ZONE_INFO');
        table.string('LOCALE');
        table.date('CREATED_AT');
        table.date('UPDATED_AT');
        table.integer('LOGIN_ATTEMPT');
        table.string('REFRESH_TOKEN');
        table.string('EMAIL_ADDRESS');
        table.integer('USER_AUTH_ID');
        table.string('USER_STATUS');
    });
};
exports.seedUserDetailTable = async () => {
    return knex('USERDETAIL').insert([
        {
            USERID: 1111, USERNAME: 'Ankur', PASSWORD: pwdEncrypted, FIRSTNAME: 'Ankur', LASTNAME: 'G',
            USERSTATUS: 'PENDING_VERIFICATION', EMAILADDRESS: 'guptaankur798@gmail.com',
            VERIFICATION_KEY: 'abc', USER_LOGIN_ID: '1111', USER_KEY: 'ag',
            CONTACTNUMBER: '9422160243', PROFILESTATUS: 'Completed', PHOTO: null, FCM_TOKEN: '',
            CREATED_AT: null, LOGIN_ATTEMPT: 0, REFRESH_TOKEN: null, EMAIL_ADDRESS: 'guptaankur798@gmail.com',
            USER_AUTH_ID: 1111, USER_STATUS: 'PENDING_VERIFICATION',
        },
        {
            USERID: 1112, USERNAME: 'Amit', PASSWORD: 'abc', FIRSTNAME: 'Amit', LASTNAME: 'B',
            USERSTATUS: 'Active', EMAILADDRESS: 'ab798@gmail.com', VERIFICATION_KEY: 'ab',
            USER_LOGIN_ID: '1112', USER_KEY: 'ab', CONTACTNUMBER: '9865745896',
            PROFILESTATUS: 'Completed', PHOTO: null, FCM_TOKEN: '', CREATED_AT: null,
            LOGIN_ATTEMPT: 0, REFRESH_TOKEN: null, EMAIL_ADDRESS: 'ab798@gmail.com',
            USER_AUTH_ID: 1112, USER_STATUS: 'Active',
        },
        {
            USERID: 1113, USERNAME: 'Dushyant', PASSWORD: 'abc', FIRSTNAME: 'Dushyant',
            LASTNAME: 'B', USERSTATUS: 'Active', EMAILADDRESS: 'db798@gmail.com',
            VERIFICATION_KEY: 'db', USER_LOGIN_ID: '1113', USER_KEY: 'db',
            CONTACTNUMBER: '78965423658', PROFILESTATUS: 'Completed', PHOTO: null, FCM_TOKEN: '',
            CREATED_AT: null, LOGIN_ATTEMPT: 0, REFRESH_TOKEN: null, EMAIL_ADDRESS: 'db798@gmail.com',
            USER_AUTH_ID: 1113, USER_STATUS: 'Active',
        },
    ]);
};
exports.seedUserAuthTable = async () => {
    return knex('USER_AUTH').insert([
        {
            USER_AUTH_ID: 1111, USERNAME: 'Ankur', PASSWORD: pwdEncrypted, VERIFICATION_KEY: 'abc',
            USER_STATUS: 'Active', LOGIN_ATTEMPT: 0, EMAIL_ADDRESS: 'guptaankur798@gmail.com',
        },
        {
            USER_AUTH_ID: 1112, USERNAME: 'Amit', PASSWORD: 'abc',
            USER_STATUS: 'Active', VERIFICATION_KEY: 'ab', LOGIN_ATTEMPT: 0, EMAIL_ADDRESS: 'ab798@gmail.com',
        },
        {
            USER_AUTH_ID: 1113, USERNAME: 'Dushyant', PASSWORD: 'abc', USER_STATUS: 'Active', VERIFICATION_KEY: 'db',
            LOGIN_ATTEMPT: 0, EMAIL_ADDRESS: 'db798@gmail.com',
        },
    ]);
};
exports.createUserAuthTable = async () => {
    return knex.schema.createTableIfNotExists('USER_AUTH', (table) => {
        table.integer('USER_AUTH_ID');
        table.string('USERNAME');
        table.string('PASSWORD');
        table.string('USER_STATUS');
        table.string('EMAIL_ADDRESS');
        table.string('VERIFICATION_KEY');
        table.string('LOGIN_ATTEMPT');
    });
};
exports.readUserDetailTable = async () => {
    return knex.withSchema('').select('*').from('USERDETAIL');
};
exports.truncateUserDetailTable = async () => {
    return knex('USERDETAIL').truncate();
};
exports.truncateUserAuthTable = async () => {
    return knex('USER_AUTH').truncate();
};
// ROLE TABLE
const createRoleTable = async () => {
    return knex.schema.createTableIfNotExists('ROLE', (table) => {
        table.integer('ROLEID');
        table.string('ROLENAME');
        table.string('CREATEDBY');
        table.string('UPDATEDBY');
        table.date('CREATIONDATE');
        table.date('UPDATIONDATE');
        table.integer('ISDELETED');
        table.date('DELETIONDATE');
        table.string('ISPROJECTLEVEL');
        table.string('ROLEDESC');
        table.integer('PROJECTID');
    });
};
const seedRoleTable = async () => {
    return knex('ROLE').insert([
        {
            ROLEID: 1, ROLENAME: 'Product Admin', CREATEDBY: '1111', UPDATEDBY: '1111',
            CREATIONDATE: '2019-02-01', UPDATIONDATE: '2019-02-01', ISDELETED: 0, DELETIONDATE: '',
            ISPROJECTLEVEL: 'Yes', ROLEDESC: 'abc', PROJECTID: '2',
        },
        {
            ROLEID: 2, ROLENAME: 'Developer', CREATEDBY: '1111', UPDATEDBY: '1111',
            CREATIONDATE: '2019-02-01', UPDATIONDATE: '2019-02-01', ISDELETED: 0, DELETIONDATE: '',
            ISPROJECTLEVEL: 'No', ROLEDESC: 'abc', PROJECTID: '2',
        },
        {
            ROLEID: 3, ROLENAME: 'Tester', CREATEDBY: '1111', UPDATEDBY: '1111',
            CREATIONDATE: '2019-02-01', UPDATIONDATE: '2019-02-01', ISDELETED: 0, DELETIONDATE: '',
            ISPROJECTLEVEL: 'No', ROLEDESC: 'abc', PROJECTID: '2',
        },
    ]);
};
const readRoleTable = async () => {
    return knex.withSchema('').select('*').from('ROLE');
};
const deleteRoleTable = async () => {
    return knex('ROLE').del();
};
// USERMAPPING TABLE
const createUserMappingTable = async () => {
    return knex.schema.createTableIfNotExists('USERMAPPING', (table) => {
        table.integer('USERMAPPINGID');
        table.integer('USERID');
        table.integer('PROJECTID');
        table.integer('ROLEID');
        table.string('ITEMID');
        table.string('MAPPINGTYPE');
        table.string('STATUS');
        table.string('DEFAULTFLAG');
    });
};
const seedUserMappingTable = async () => {
    return knex('USERMAPPING').insert([
        {
            USERMAPPINGID: 1, USERID: 1111, PROJECTID: 0, ROLEID: 1, ITEMID: 'aa',
            MAPPINGTYPE: 'USER_PROJECT', STATUS: 'Active', DEFAULTFLAG: 'Y',
        },
        {
            USERMAPPINGID: 2, USERID: 1111, PROJECTID: 0, ROLEID: 1, ITEMID: 'bb',
            MAPPINGTYPE: 'USER_PROJECT_ROLE', STATUS: 'Active', DEFAULTFLAG: 'Y',
        },
        {
            USERMAPPINGID: 3, USERID: 1114, PROJECTID: 0, ROLEID: 2, ITEMID: 'cc',
            MAPPINGTYPE: 'USER_PROJECT', STATUS: 'Active', DEFAULTFLAG: 'Y',
        },
    ]);
};
const readUserMappingTable = async () => {
    return knex.withSchema('').select('*').from('USERMAPPING');
};
const deleteUserMappingTable = async () => {
    return knex('USERMAPPING').del();
};
exports.tableSetup = async () => {
    const userTableCreated = await exports.createUserDetailTable();
    const userTableSeeded = await exports.seedUserDetailTable();
    const userAuthCreated = await exports.createUserAuthTable();
    const userAuthSeeded = await exports.seedUserAuthTable();
    const roleTableCreated = await createRoleTable();
    const roleTableSeeded = await seedRoleTable();
    const userMappingTableCreated = await createUserMappingTable();
    const userMappingTableSeeded = await seedUserMappingTable();
};
//# sourceMappingURL=user.sqlitedb.js.map