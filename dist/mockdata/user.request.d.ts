declare const _default: {
    createUserRequest: {
        userId: string;
        userName: string;
        password: string;
        firstName: string;
        lastName: string;
        emailAddress: string;
    };
    updateUserRequest: {
        lastName: string;
        emailAddress: string;
    };
    updateAllUsersRequest: {
        password: string;
    };
    createRoleRequest: {
        roleId: string;
        roleName: string;
    };
    updateRoleRequest: {
        roleName: string;
    };
};
export default _default;
//# sourceMappingURL=user.request.d.ts.map